# CBS Python
This package includes tools for working with data from Cambridge Brain Sciences (CBS). There are a few different features that you might find useful:
- Parsing raw CBS data into a useable/organized form
- Performing calculations involving normative data
- Score calculations
- Generating indexes / lists for querying score dataframes.
- Others? (Still working on updating this document!)

# Dependencies
- Pandas
- Numpy
- ...

# Installation
1. Clone this repository somewhere on your local machine.
2. Go the root directory of this repo, then `pip3 install ./` (If you want to be able to make changes, then add a `-e`).

# Useage
In your python script (or program, whatever) import the library:
`import cbspython as cbs`

## Parsing Raw Data
A CBS `Report` object is basically a wrapper for a Pandas DataFrame that includes helper functions for pivoting the longform data into wideform. Creating a new `Report` object parses an existing raw CBS datasource (either a string indicating the full path to a .CSV file, or a pd.DataFrame object in memory). For example:

- `my_data = cbs.Report('~/raw_data_from_CBS_admin_portal.csv', n_jobs=-1)`

Parsing the raw data can be time consuming if there are thousands of raw scores, so it is recommended that you pickle the resulting Report object in order to load it again more quickly.

Note, parsing the raw data *requires* certain columns to be present: 
`['Id', 'User', 'User Email', 'Test', 'Test Name', 'Trial Name', 'Batch', 'Batch Name', 'Time Point', 'Screen Width', 'Created At', 'Score', 'Valid', 'Session ID', 'Locale', 'Session Data', 'Legacy Raw Score']`
