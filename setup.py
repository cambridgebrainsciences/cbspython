from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="cbspython",
    version="0.2.1",
    author="Conor J. Wild",
    author_email="conor.wild@cambridgebrainsciences.com",
    description="A library of useful python tools for CBS datasets",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/cambridgebrainsciences/cbspython/src/main/",
    packages=find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=["numpy", "pandas>= 1.3.3", "scipy", "argparse"],
    extras_require={
        "test": ["pytest~=7.2.0", "pytest-cov", "pytest-mock", "pytest-cases"],
    },
    entry_points="""
        [console_scripts]
        cbs_score_calculator = cbspython.scripts.score_calculator:main
        cbs_generate_norms = cbspython.scripts.generate_norms:main
        cbs_parse_data = cbspython.scripts.parse_raw_data:main
    """,
)
