import numpy as np
import pandas as pd
import re
import json


class Test(object):
    _NUM_RAW_FIELDS = 14
    _FEATURE_FIELD_NAMES = ["single_score"] + [
        "raw_field_%02d" % (i + 1) for i in range(_NUM_RAW_FIELDS)
    ]
    _id = None
    _name = ""
    _abbrev = ""
    _features = ["final_score"]
    _hc_feature = "final_score"
    _domain_feature = "final_score"
    _chance = 0
    _r_hc = 0

    def __init__(self):
        score_features = {
            self.features()[i]: self._FEATURE_FIELD_NAMES[i]
            for i in range(len(self.features()))
        }
        common_features = {x: x for x in self.common_session_features()}
        self._feature_map = {**score_features, **common_features}

    @property
    def name(self):
        """The name of the test."""
        return self._name

    @property
    def id(self):
        """The ID number for this tests. Corresponds to the MySQL ID."""
        return self._id

    @property
    def abbrev(self):
        """Abbreviated version of the test name."""
        return self._abbrev

    @property
    def chance_level(self):
        """Every test has an associated "chance" level of performance."""
        return self._chance

    @property
    def reliability(self, app="hc"):
        """Reliability, based on CBS app, of the HC feature."""
        if app == "hc":
            return self._r_hc

    def features(self, include_common=False):
        """A list of all features (names) for scores from this test."""
        if include_common:
            return self._features + self.common_session_features()
        else:
            return self._features

    def num_features(self, include_common=False):
        """The number of features for scores from this Test."""
        if include_common:
            return len(self._features) + len(self.common_session_features())
        else:
            return len(self._features)

    @property
    def feature_fields(self):
        """A list of all score fields that contain features."""
        return self._FEATURE_FIELD_NAMES[0 : self.num_features(include_common=False)]

    @property
    def hc_feature(self):
        """The name of the feature used for HC score calculations."""
        return self._hc_feature

    @property
    def hc_field(self):
        """The score field that contains the feature used for HC score
        calculations.
        """
        return self.field_for_feature(self.hc_feature)

    @property
    def domain_feature(self):
        """The name of the feature used for domain score calculations."""
        return self._domain_feature

    @property
    def domain_field(self):
        """The score field that contains the feature used for domain score
        calculations.
        """
        return self.field_for_feature(self.domain_feature)

    def field_for_feature(self, feature_name):
        """Given the name of a score feature, returns the score field that
            contains that data.

        Args:
            feature_name: the name of the feature (as a string)
        Returns:
            The corresponding field name, as a string.
        """
        return self._feature_map[feature_name]

    def parse_raw(self, raw_score_str):
        return {}

    def parse_legacy_raw(self, legacy_raw_str):
        return {}

    @classmethod
    def common_session_feature_calcs(self):
        """This dict maps scores features (names) to function calls.
        Keys are the names of features, values are the names of the
        function that uses the session data to calculate the feature value.
        """
        return {
            "num_errors": "num_errors_from_session",
            "num_correct": "num_correct_from_session",
            "num_attempts": "num_attempts_from_session",
            "accuracy": "accuracy_from_session",
            "better_than_chance": "better_than_chance_from_session",
            "duration_ms": "duration_from_session",
            "avg_ms_correct": "avg_ms_correct_from_session",
            "std_ms_correct": "std_ms_correct_from_session",
        }

    @classmethod
    def common_session_features(self):
        """Returns a list of features (names) that are derived from session
        data, and that are common to all tests. This is a classmethod,
        because it is like a property of the Test class, rather than an
        instance of a specific Test.
        """
        return list(self.common_session_feature_calcs().keys())

    @property
    def test_specific_session_feature_calcs(self):
        return {}

    @property
    def all_session_feature_calcs(self):
        return {
            **self.common_session_feature_calcs(),
            **self.test_specific_session_feature_calcs,
        }

    @property
    def all_session_features(self):
        return list(self.all_session_feature_calcs.keys())

    def parse_session(self, session_data_str, extract_features=True):
        try:
            session_data = json.loads(session_data_str)
        except Exception:
            print(f"WARNING: Cannot parse JSON session {session_data_str}")
            return {}

        if session_data["code"] != self.abbrev.lower():
            print(
                (
                    f"WARNING: Invalid Task Code, got {session_data['code']} "
                    f"expected {self.abbrev.lower()}"
                )
            )
            return {}

        if extract_features:
            return {
                feature: getattr(self, function)(session_data)
                for feature, function in self.all_session_feature_calcs.items()
            }

        return session_data

    def correct_trials(self, session_data):
        """Returns a list of the correct trials from the parsed session_data"""
        return [q for q in self.all_trials(session_data) if q["isCorrect"]]

    def all_trials(self, session_data):
        return [
            q
            for q in session_data["questions"]
            if ("isCorrect" in q) and ("endTimeSpan" in q)
        ]

    def question_difficulty(self, session_data):
        return np.nan

    def extract_trial_data(self, session_data):
        trials = []
        if (session_data is not None) and ("questions" in session_data):
            for q in session_data["questions"]:
                if "endTimeSpan" in q:
                    trials.append(
                        [
                            bool(q["isCorrect"]),
                            self.question_difficulty(q),
                            q["durationTimeSpan"],
                        ]
                    )
        return pd.DataFrame(
            trials, columns=["correct", "difficulty", "duration"]
        ).rename_axis("trial")

    def num_errors_from_session(self, session_data):
        return session_data["errorsMade"]

    def num_correct_from_session(self, session_data):
        return session_data["correctAnswers"]

    def num_attempts_from_session(self, session_data):
        return session_data["errorsMade"] + session_data["correctAnswers"]

    def accuracy_from_session(self, session_data):
        total = self.num_attempts_from_session(session_data)
        correct = self.num_correct_from_session(session_data)
        return correct / total if total != 0 else np.nan

    def better_than_chance_from_session(self, session_data):
        accuracy = self.accuracy_from_session(session_data)
        return accuracy > self.chance_level

    def duration_from_session(self, session_data):
        return session_data["task"]["durationTimeSpan"]

    def durations_of_correct_trials(self, session_data):
        return np.array(
            [q["durationTimeSpan"] for q in self.correct_trials(session_data)]
        )

    def avg_ms_correct_from_session(self, session_data):
        durations = self.durations_of_correct_trials(session_data)
        return durations.mean() if durations.shape[0] > 0 else np.nan

    def std_ms_correct_from_session(self, session_data):
        durations = self.durations_of_correct_trials(session_data)
        return durations.std() if durations.shape[0] > 0 else np.nan

    @property
    def calculated_features(self):
        return {}

    def calculate_features(self, feature_data):
        return {
            feature: getattr(self, function)(feature_data)
            for feature, function in self.calculated_features.items()
        }


class MemoryTest(Test):

    _features = ["max_score", "avg_score", "avg_ms_per_item"]
    _hc_feature = "avg_score"
    _domain_feature = "max_score"

    """ Memory Tests have a consistent raw score format """

    def parse_raw(self, raw_score_str):
        """For memory tests, the raw score is a floating point number, which
        is just the average score feature. However, cases where the score
        is actually zero (0), the raw field contains an empty string rather
        than zero; check for this possibility."""
        if raw_score_str == "":
            avg_score = 0
        else:
            avg_score = float(raw_score_str)
        return {"avg_score": avg_score}

    @property
    def test_specific_session_feature_calcs(self):
        return {"avg_ms_per_item": "calc_avg_ms_per_item"}

    def calc_avg_ms_per_item(self, session_data):
        item_times = [
            q["durationTimeSpan"] / self.question_difficulty(q)
            for q in self.correct_trials(session_data)
        ]
        return np.array(item_times).mean()


class SpatialSpanTest(MemoryTest):
    _id = 20
    _name = "spatial_span"
    _abbrev = "SS"
    _r_hc = 0.603409

    def question_difficulty(self, q):
        return len(q["question"])


class DigitSpanTest(MemoryTest):
    _id = 17
    _name = "digit_span"
    _abbrev = "DS"
    _r_hc = 0.608394

    def question_difficulty(self, q):
        return q["attributes"]["score"]


class MonkeyLadderTest(SpatialSpanTest):
    _id = 24
    _name = "monkey_ladder"
    _abbrev = "ML"
    _r_hc = 0.584249

    def question_difficulty(self, q):
        return len(q["question"])


class PairedAssociatesTest(SpatialSpanTest):
    _id = 19
    _name = "paired_associates"
    _abbrev = "PA"
    _r_hc = 0.488574

    def question_difficulty(self, q):
        return len(q["question"])


class TokenSearchTest(MemoryTest):
    _id = 18
    _name = "token_search"
    _abbrev = "TS"
    _r_hc = 0.596558

    def question_difficulty(self, q):
        return q["attributes"]["0"]["difficultyLevel"]


class ReasoningTest(Test):
    _features = ["final_score", "attempted", "errors", "max", "correct_score"]
    _hc_feature = "final_score"
    _domain_feature = "final_score"
    _chance = 0.5

    # Reasoning Tests have a consistent raw score format
    _raw_str_regexp = re.compile(
        r"Attempted\s+(?P<attempted>-?\d+)\s+"
        r"Errors\s+(?P<errors>-?\d+)\s+"
        r"Max\s+(?P<max>-?\d+)\s+"
        r"Score\s+(?P<score>-?\d+)\s+"
        r"CorrectScore\s+(?P<correct_score>-?\d+)"
    )

    def parse_raw(self, raw_score_str):
        """Use a regexp to parse out keyword separated feature values"""
        m = re.search(self._raw_str_regexp, raw_score_str)
        return {feature: float(value) for (feature, value) in m.groupdict().items()}


class RotationsTest(ReasoningTest):
    _id = 22
    _name = "rotations"
    _abbrev = "RT"
    _r_hc = 0.600405

    def question_difficulty(self, q):
        return q["difficulty"]


class FeatureMatchTest(ReasoningTest):
    _id = 21
    _name = "feature_match"
    _abbrev = "FM"
    _r_hc = 0.56137

    def question_difficulty(self, q):
        return q["difficulty"]


class OddOneOutTest(ReasoningTest):
    _id = 14
    _name = "odd_one_out"
    _abbrev = "OOO"
    _features = ["final_score", "attempted", "errors", "max"]
    _domain_feature = "max"
    _chance = 1.0 / 9
    _r_hc = 0.454132

    """ OddOneOut has a slightly different slightly different raw format """
    _raw_str_regexp = re.compile(
        r"Attempted\s+(?P<attempted>-?\d+)\s+"
        r"Errors\s+(?P<errors>-?\d+)\s+"
        r"Max\s+(?P<max>-?\d+)"
    )

    def question_difficulty(self, q):
        return q["attributes"]["realDifficulty"]


class DoubleTroubleTest(ReasoningTest):
    _id = 13
    _name = "double_trouble"
    _abbrev = "DT"
    _features = [
        "final_score",
        "pct_CC",
        "pct_CI",
        "pct_IC",
        "pct_II",
        "RT_CC",
        "RT_CI",
        "RT_IC",
        "RT_II",
        "IR_RT",
        "IR_errors",
        "ncorrect_CC",
        "ncorrect_II",
    ]
    _r_hc = 0.822693

    """ DT has a very different raw format """
    _raw_str_regexp = re.compile(
        r"\A(?P<pct_CC>\S+)\s+"
        r"(?P<pct_CI>\S+)\s+"
        r"(?P<pct_IC>\S+)\s+"
        r"(?P<pct_II>\S+)\s+"
        r"(?P<RT_CC>\S+)\s+"
        r"(?P<RT_CI>\S+)\s+"
        r"(?P<RT_IC>\S+)\s+"
        r"(?P<RT_II>\S+)"
    )

    def question_difficulty(self, q):
        return {"CC": 0, "CI": 1, "IC": 1, "II": 2}[q["problemCode"]]

    @property
    def test_specific_session_feature_calcs(self):
        return {"qs_by_type": "qs_by_type"}

    def qs_by_type(self, session_data):
        qs = {"CC": [], "CI": [], "IC": [], "II": []}
        for q in self.all_trials(session_data):
            qs[q["problemCode"]].append(q)

        return qs

    def calc_IR_errors(self, feature_data):
        X = np.vstack((np.ones(3), (-1, 0, 1))).T
        y = np.array(
            [feature_data["pct_CC"], feature_data["pct_IC"], feature_data["pct_II"]]
        )
        beta_hat = np.linalg.pinv(X).dot(y)
        return beta_hat[1]

    def calc_IR_RT(self, feature_data):
        X = np.vstack((np.ones(3), (-1, 0, 1))).T
        y = np.array(
            [feature_data["RT_CC"], feature_data["RT_IC"], feature_data["RT_II"]]
        )
        beta_hat = np.linalg.pinv(X).dot(y)
        return beta_hat[1]

    def calc_ncorrect_CC(self, feature_data):
        all_qs = feature_data["qs_by_type"]
        return len([q for q in all_qs["CC"] if q["isCorrect"]])

    def calc_ncorrect_II(self, feature_data):
        all_qs = feature_data["qs_by_type"]
        return len([q for q in all_qs["II"] if q["isCorrect"]])

    @property
    def calculated_features(self):
        return {
            "IR_errors": "calc_IR_errors",
            "IR_RT": "calc_IR_RT",
            "ncorrect_CC": "calc_ncorrect_CC",
            "ncorrect_II": "calc_ncorrect_II",
        }


class PolygonsTest(Test):
    _id = 23
    _name = "polygons"
    _abbrev = "PO"
    _chance = 0.5
    _r_hc = 0.523616

    def question_difficulty(self, q):
        return q["difficulty"]


class SpatialPlanningTest(Test):
    _id = 15
    _name = "spatial_planning"
    _abbrev = "SP"
    _r_hc = 0.731601

    def question_difficulty(self, q):
        return q["attributes"]["difficultyLevel"]


class GrammaticalReasoningTest(Test):
    _id = 16
    _name = "grammatical_reasoning"
    _abbrev = "GR"
    _chance = 0.5
    _r_hc = 0.758347

    questions = [
        {
            "text": "square is bigger than circle",
            "outer": "square",
            "answer": True,
            "difficulty": 1,
        },
        {
            "text": "square is smaller than circle",
            "outer": "square",
            "answer": False,
            "difficulty": 2,
        },
        {
            "text": "square contains circle",
            "outer": "square",
            "answer": True,
            "difficulty": 0,
        },
        {
            "text": "square is contained by circle",
            "outer": "square",
            "answer": False,
            "difficulty": 2,
        },
        {
            "text": "square encapsulates circle",
            "outer": "square",
            "answer": True,
            "difficulty": 1,
        },
        {
            "text": "square is encapsulated by circle",
            "outer": "square",
            "answer": False,
            "difficulty": 3,
        },
        {
            "text": "square is not bigger than circle",
            "outer": "square",
            "answer": False,
            "difficulty": 3,
        },
        {
            "text": "square is not smaller than circle",
            "outer": "square",
            "answer": True,
            "difficulty": 2,
        },
        {
            "text": "square does not contain circle",
            "outer": "square",
            "answer": False,
            "difficulty": 2,
        },
        {
            "text": "square is not contained by circle",
            "outer": "square",
            "answer": True,
            "difficulty": 2,
        },
        {
            "text": "square does not encapsulate circle",
            "outer": "square",
            "answer": False,
            "difficulty": 3,
        },
        {
            "text": "square is not encapsulated by circle",
            "outer": "square",
            "answer": True,
            "difficulty": 3,
        },
        {
            "text": "circle is bigger than square",
            "outer": "square",
            "answer": False,
            "difficulty": 2,
        },
        {
            "text": "circle is smaller than square",
            "outer": "square",
            "answer": True,
            "difficulty": 1,
        },
        {
            "text": "circle contains square",
            "outer": "square",
            "answer": False,
            "difficulty": 1,
        },
        {
            "text": "circle is contained by square",
            "outer": "square",
            "answer": True,
            "difficulty": 1,
        },
        {
            "text": "circle encapsulates square",
            "outer": "square",
            "answer": False,
            "difficulty": 2,
        },
        {
            "text": "circle is encapsulated by square",
            "outer": "square",
            "answer": True,
            "difficulty": 2,
        },
        {
            "text": "circle is not bigger than square",
            "outer": "square",
            "answer": True,
            "difficulty": 2,
        },
        {
            "text": "circle is not smaller than square",
            "outer": "square",
            "answer": False,
            "difficulty": 3,
        },
        {
            "text": "circle does not contain square",
            "outer": "square",
            "answer": True,
            "difficulty": 1,
        },
        {
            "text": "circle is not contained by square",
            "outer": "square",
            "answer": False,
            "difficulty": 3,
        },
        {
            "text": "circle does not encapsulate square",
            "outer": "square",
            "answer": True,
            "difficulty": 2,
        },
        {
            "text": "circle is not encapsulated by square",
            "outer": "square",
            "answer": False,
            "difficulty": 4,
        },
        {
            "text": "square is bigger than circle",
            "outer": "circle",
            "answer": False,
            "difficulty": 2,
        },
        {
            "text": "square is smaller than circle",
            "outer": "circle",
            "answer": True,
            "difficulty": 1,
        },
        {
            "text": "square contains circle",
            "outer": "circle",
            "answer": False,
            "difficulty": 1,
        },
        {
            "text": "square is contained by circle",
            "outer": "circle",
            "answer": True,
            "difficulty": 1,
        },
        {
            "text": "square encapsulates circle",
            "outer": "circle",
            "answer": False,
            "difficulty": 2,
        },
        {
            "text": "square is encapsulated by circle",
            "outer": "circle",
            "answer": True,
            "difficulty": 2,
        },
        {
            "text": "square is not bigger than circle",
            "outer": "circle",
            "answer": True,
            "difficulty": 2,
        },
        {
            "text": "square is not smaller than circle",
            "outer": "circle",
            "answer": False,
            "difficulty": 3,
        },
        {
            "text": "square does not contain circle",
            "outer": "circle",
            "answer": True,
            "difficulty": 1,
        },
        {
            "text": "square is not contained by circle",
            "outer": "circle",
            "answer": False,
            "difficulty": 3,
        },
        {
            "text": "square does not encapsulate circle",
            "outer": "circle",
            "answer": True,
            "difficulty": 2,
        },
        {
            "text": "square is not encapsulated by circle",
            "outer": "circle",
            "answer": False,
            "difficulty": 4,
        },
        {
            "text": "circle is bigger than square",
            "outer": "circle",
            "answer": True,
            "difficulty": 1,
        },
        {
            "text": "circle is smaller than square",
            "outer": "circle",
            "answer": False,
            "difficulty": 2,
        },
        {
            "text": "circle contains square",
            "outer": "circle",
            "answer": True,
            "difficulty": 0,
        },
        {
            "text": "circle is contained by square",
            "outer": "circle",
            "answer": False,
            "difficulty": 2,
        },
        {
            "text": "circle encapsulates square",
            "outer": "circle",
            "answer": True,
            "difficulty": 1,
        },
        {
            "text": "circle is encapsulated by square",
            "outer": "circle",
            "answer": False,
            "difficulty": 3,
        },
        {
            "text": "circle is not bigger than square",
            "outer": "circle",
            "answer": False,
            "difficulty": 3,
        },
        {
            "text": "circle is not smaller than square",
            "outer": "circle",
            "answer": True,
            "difficulty": 2,
        },
        {
            "text": "circle does not contain square",
            "outer": "circle",
            "answer": False,
            "difficulty": 2,
        },
        {
            "text": "circle is not contained by square",
            "outer": "circle",
            "answer": True,
            "difficulty": 2,
        },
        {
            "text": "circle does not encapsulate square",
            "outer": "circle",
            "answer": False,
            "difficulty": 3,
        },
        {
            "text": "circle is not encapsulated by square",
            "outer": "circle",
            "answer": True,
            "difficulty": 3,
        },
    ]

    # USED this to generate the above dict list
    # ds = []
    # for q in gr.questions:
    #     d1 = [v for k,v in gr.difficulties.items() if k in q['text']][0]
    #     d2 = 0 if q['answer'] else 1
    #     print(f"{{ 'text':'{q['text']}', 'outer': '{q['outer']}',
    #               'answer': {str(q['answer'])}, 'difficulty': {d1+d2}}}")

    difficulties = {
        "contains": 0,  # active  + positive
        "does not contain": 1,  # active  + negative
        "is contained by": 1,  # passive + positive
        "is not contained by": 2,  # passive + negative
        "encapsulates": 1,  # active  + positive (harder word)
        "does not encapsulate": 2,  # active  + negatuve (harder word)
        "is encapsulated by": 2,  # passive + positive (harder word)
        "is not encapsulated by": 3,  # passive + negative (harder word)
        "is bigger than": 1,  # passive + positive
        "is not bigger than": 2,  # passive + negative
        "is smaller than": 1,  # passive + positive
        "is not smaller than": 2,  # passive + negative
    }

    def question_difficulty(self, parsed_question):
        """Returns the difficulty of the question. Depends on the syntax,
        and whether or not the question is true, etc.
        """
        return [
            q
            for q in self.questions
            if (parsed_question["questionText"] == q["text"])
            and (parsed_question["outer"] == q["outer"])
        ][0]["difficulty"]


class SARTest(Test):
    _id = 25
    _name = "sar"
    _abbrev = "SAR"
    _features = [
        "final_score",
        "nogo_success",
        "go_success",
        "num_commissions",
        "pct_commissions",
        "num_omissions",
        "pct_omissions",
        "num_anticipatory",
        "num_ambiguous",
        "avg_RT_go",
        "std_RT_go",
        "RT_cv",
        "avg_RT_b4_nogo_success",
        "avg_RT_b4_nogo_fail",
        "post_CE_slowing",
    ]

    _domain_feature = None
    _chance = None
    _r_hc = None

    _raw_str_regexp = re.compile(
        r"\n(\d+(\.\d+)?)"
        r"\t(?P<go_success>\d+(\.\d+)?)"
        r"\t(?P<num_commissions>\d+(\.\d+)?)"
        r"\t(?P<pct_commissions>\d+(\.\d+)?)"
        r"\t(?P<num_omissions>\d+(\.\d+)?)"
        r"\t(?P<pct_omissions>\d+(\.\d+)?)"
        r"\t(?P<num_anticipatory>\d+(\.\d+)?)"
        r"\t(?P<num_ambiguous>\d+(\.\d+)?)"
        r"\t(?P<avg_RT_go>\d+(\.\d+)?)"
        r"\t(?P<std_RT_go>\d+(\.\d+)?)"
        r"\t(?P<RT_cv>\d+(\.\d+)?)"
        r"\t(?P<avg_RT_b4_nogo_success>\d+(\.\d+)?)"
        r"\t(?P<avg_RT_b4_nogo_fail>\d+(\.\d+)?)"
    )

    _RT_ANTICIPATORY = 100
    _RT_AMBIGUOUS = 438

    def parse_raw(self, raw_score_str):
        """Use a regexp to parse out keyword separated feature values"""
        m = re.search(self._raw_str_regexp, raw_score_str)
        return {feature: float(value) for (feature, value) in m.groupdict().items()}

    @classmethod
    def common_session_feature_calcs(self):
        """This dict maps scores features (names) to function calls.
        Keys are the names of features, values are the names of the
        function that uses the session data to calculate the feature value.
        """
        return {
            "num_errors": "num_errors_from_session",
            "num_correct": "num_correct_from_session",
            "num_attempts": "num_attempts_from_session",
            "duration_ms": "duration_from_session",
        }

    @property
    def test_specific_session_feature_calcs(self):
        return {
            "trials": "all_trials",
        }

    @property
    def calculated_features(self):
        return {
            "nogo_success": "calc_nogo_success",
            "post_CE_slowing": "calc_post_commission_slowing",
        }

    def calc_nogo_success(self, feature_data):
        return len([t for t in feature_data["trials"] if self.is_nogo_success(t)])

    @classmethod
    def is_commission_error(cls, t):
        return (t["attributes"]["isNoGo"]) & (not t["isCorrect"])

    @classmethod
    def is_ommission_error(cls, t):
        return (not t["attributes"]["isNoGo"]) & (not t["isCorrect"])

    @classmethod
    def is_nogo_trial(self, t):
        return t["attributes"]["isNoGo"]

    @classmethod
    def is_nogo_success(cls, t):
        return (t["attributes"]["isNoGo"]) & (t["isCorrect"])

    @classmethod
    def is_go_trial(cls, t):
        return not cls.is_nogo_trial(t)

    @classmethod
    def is_go_success(cls, t):
        return (
            cls.is_go_trial(t)
            & t["isCorrect"]
            & (t["attributes"].get("answerTimeSpan", np.nan) > cls._RT_AMBIGUOUS)
        )

    def calc_post_commission_slowing(self, feature_data):
        """
        “We compared ADHD and NC participants on the difference between
         median RTs in the trial immediately after a commission error and the
         median RTs of correct responses. On this parameter, the children with
         ADHD slowed down significantly less than NC participants.”
        """
        post_CE_RTs = []
        correct_go_RTs = []
        prev_trial_CE = False
        for i, t in enumerate(feature_data["trials"]):
            if self.is_commission_error(t):
                prev_trial_CE = True
            else:
                if prev_trial_CE:
                    if "answerTimeSpan" in t["attributes"]:
                        post_CE_RTs.append(t["attributes"]["answerTimeSpan"])
                elif self.is_go_success(t):
                    correct_go_RTs.append(t["attributes"]["answerTimeSpan"])
                prev_trial_CE = False
        return np.median(post_CE_RTs) - np.median(correct_go_RTs)


TESTS = {
    "spatial_span": SpatialSpanTest(),
    "grammatical_reasoning": GrammaticalReasoningTest(),
    "double_trouble": DoubleTroubleTest(),
    "odd_one_out": OddOneOutTest(),
    "monkey_ladder": MonkeyLadderTest(),
    "rotations": RotationsTest(),
    "feature_match": FeatureMatchTest(),
    "digit_span": DigitSpanTest(),
    "spatial_planning": SpatialPlanningTest(),
    "paired_associates": PairedAssociatesTest(),
    "polygons": PolygonsTest(),
    "token_search": TokenSearchTest(),
    "sar": SARTest(),
}

HC_TESTS = {
    "spatial_span": SpatialSpanTest(),
    "grammatical_reasoning": GrammaticalReasoningTest(),
    "double_trouble": DoubleTroubleTest(),
    "odd_one_out": OddOneOutTest(),
    "monkey_ladder": MonkeyLadderTest(),
    "rotations": RotationsTest(),
    "feature_match": FeatureMatchTest(),
    "digit_span": DigitSpanTest(),
    "spatial_planning": SpatialPlanningTest(),
    "paired_associates": PairedAssociatesTest(),
    "polygons": PolygonsTest(),
    "token_search": TokenSearchTest(),
}


def test_names(exclude=[]):
    return [t for t in list(TESTS.keys()) if t not in exclude]


def all_feature_fields(include_common=False, inject=[]):
    """Returns a list of tuples, where the first tuple element is the test
        name, and the second tuple  value is a feature field name. Useful for
        subselecting the partition of a full score data frame that contains
        data.

    Args:
        include_common (boolean): include all the common session-derived
            features for every test? e.g., num correct, num errors, etc.
            (default: False)
        inject (list): a list of other field or variable names to include in
            the list of fields for every test.
    """
    all_fields = []
    for test_name, test in TESTS.items():
        test_fields = test.feature_fields
        if include_common:
            test_fields += test.common_session_features()
        all_fields += [(test_name, field) for field in test_fields]
        all_fields += [(test_name, label) for label in inject]
    return all_fields


def all_features(include_common=False, exclude=[], only=None, inject=[]):
    """Returns a list of tuples, where the first tuple element is the test
        name, and the second tuple value is a feature name. Useful for
        subselecting the partition of a full score data frame that contains
        data.
    Args:
        include_common: (default True) should we include all features that are
            common for all tests? e.g., num attempts, errors, etc.
        exclude: (list-like, default empty list) remove any features that match
            anything in the list. That is, filter out some features by name.
        only: (list-like, default empty list) retain only features that match
            anything in this list. Kind of the opposite of 'exclude'.
        inject (list): a list of other features or variable names to include in
            the list of features for every test.
    """
    features = []
    for test_name, test in TESTS.items():
        for feature in test.features(include_common) + inject:
            features.append((test_name, feature))

    filtered = []
    for feature in features:
        if (feature[1] not in exclude) and (only is None or feature[1] in only):
            filtered.append(feature)
    return filtered


def timing_features(exclude, abbrev=False):
    """Returns all reaction-time based measures for each test."""
    exclude = exclude + ["sar"]

    def timing_feat(test):
        if test in memory_tests():
            return "avg_ms_per_item"
        else:
            return "avg_ms_correct"

    feats = [
        (test, timing_feat(test)) for test, _ in HC_TESTS.items() if test not in exclude
    ]
    if abbrev:
        feats = abbrev_features(feats)
    return feats


def abbrev_features(feature_list):
    """Given a list of test score features as a tuple (used for multiindex),
    translate into a single level list using abbreviated test names.
    """
    return [f"{TESTS[t[0]].abbrev}_{t[1]}" for t in feature_list]


def abbrev_columns(df):
    """Given a DataFrame, abbreviate the columns from a multiIndex into
    underscore-separate strings. e.g., (digit_span, max_score) would
    become "DS_max_score"
    """
    df.columns = abbrev_features(list(df.columns))
    return df


def test_features(app="domain", type_="feature", abbreviated=False):
    """Generate a list of test score features used to calculate score for a
        given application. This is useful for indexing complete score dataframe
        to select only features used in that application.
    Arguments:
        app: (string-like, default 'domain') can be 'domain' or 'hc'
        type_: (string-like, default 'feature') can be 'feature' or 'field'
        abbreviated: (boolean, default False) return the abbreviated version
            of test score features? e.g., 'DS_max_score' instead of
            ('digit_span, 'max_score).
    Returns:
        [list of tuples] - Each is of the form (test, feature)
    """
    if app not in ["domain", "hc"]:
        raise ValueError(f"Invalid application {app}")

    if type_ not in ["feature", "field"]:
        raise ValueError(f"Invalid argument {type_}")

    if app == "hc":
        test_set = HC_TESTS
    elif app == "domain":
        test_set = HC_TESTS  # for now because not sure how to handle SAR
    else:
        raise ValueError(f"Invalid application {app}")

    features = [
        (test_name, getattr(test, f"{app}_{type_}"))
        for (test_name, test) in test_set.items()
    ]
    if abbreviated:
        features = abbrev_features(features)
    return features


def domain_feature_list(abbreviated=False):
    return test_features("domain", "feature", abbreviated)


def hc_feature_list(abbreviated=False):
    return test_features("hc", "feature", abbreviated)


def tests_of_type(type_, exclude=[]):
    parent_class = {"memory": MemoryTest, "reasoning": ReasoningTest}.get(type_, None)

    if parent_class is None:
        raise AttributeError(f"Invalid test type {type_}")

    return [
        name
        for (name, test) in TESTS.items()
        if issubclass(test.__class__, parent_class) and name not in exclude
    ]


def memory_tests(exclude=[]):
    return tests_of_type("memory", exclude)


def reasoning_tests(exclude=[]):
    return tests_of_type("reasoning", exclude)


def set_column_names(df, names):
    """Give a dataframe, simply assigns new names to the columns."""
    assert len(names) == len(df.columns)
    df.columns = names
    return df


def abbreviate_columns(df):
    """Given a CBS dataframe with MultiIndex column names, reassign to the
    abbreviated versions.
    """
    return set_column_names(df, abbrev_features(df.columns))


ROTATED_PCA_LOADINGS = np.array(
    [
        [0.7014310832595965, 0.2114602614099599, 0.06335835494755199],
        [0.06489246556607997, 0.3200071012586953, 0.6555755842264829],
        [0.21528601455159402, 0.3526924332953027, 0.5250830836410911],
        [0.1992332685797254, 0.5526963820789189, -0.11183812310895136],
        [0.6968350824881397, 0.2277093563746992, 0.053842026805891285],
        [0.12127517611619794, 0.6653527253557938, 0.09299029746874055],
        [0.16663501355802068, 0.5504977638792241, 0.2190648077992447],
        [0.2697350970669865, -0.20477442132929694, 0.7086057589964939],
        [0.4015791806929251, 0.45692510567203554, 0.03807692316482178],
        [0.5987583784444802, -0.03013309539255579, 0.23366793983112322],
        [-0.00047574212977022357, 0.5198168539290584, 0.3378749934189247],
        [0.6001410364160199, 0.1816371349537731, 0.18355395542544883],
    ]
)

DOMAIN_NAMES = ["STM", "reasoning", "verbal"]

COVARIANCE = np.array(
    [
        [
            1.00001877,
            0.17657497,
            0.25456675,
            0.15975593,
            0.41123286,
            0.22052783,
            0.23950432,
            0.16941961,
            0.25657813,
            0.24761691,
            0.1748068,
            0.33267418,
        ],
        [
            0.17657497,
            1.00001877,
            0.31134129,
            0.13863341,
            0.20351513,
            0.20829056,
            0.22292791,
            0.21532425,
            0.19143197,
            0.16329805,
            0.18374061,
            0.19766977,
        ],
        [
            0.25456675,
            0.31134129,
            1.00001877,
            0.12745101,
            0.24312973,
            0.21874051,
            0.24699616,
            0.18504348,
            0.24972915,
            0.18741918,
            0.20326165,
            0.25492511,
        ],
        [
            0.15975593,
            0.13863341,
            0.12745101,
            1.00001877,
            0.16839422,
            0.20850424,
            0.18164247,
            0.05334255,
            0.17095683,
            0.10991068,
            0.13512469,
            0.12871365,
        ],
        [
            0.41123286,
            0.20351513,
            0.24312973,
            0.16839422,
            1.00001877,
            0.20693492,
            0.23425227,
            0.17376448,
            0.27081193,
            0.25982165,
            0.15926039,
            0.31415826,
        ],
        [
            0.22052783,
            0.20829056,
            0.21874051,
            0.20850424,
            0.20693492,
            1.00001877,
            0.26734509,
            0.06536186,
            0.2462393,
            0.13388592,
            0.22042753,
            0.17894845,
        ],
        [
            0.23950432,
            0.22292791,
            0.24699616,
            0.18164247,
            0.23425227,
            0.26734509,
            1.00001877,
            0.1109622,
            0.21611095,
            0.16251798,
            0.23562067,
            0.20001099,
        ],
        [
            0.16941961,
            0.21532425,
            0.18504348,
            0.05334255,
            0.17376448,
            0.06536186,
            0.1109622,
            1.00001877,
            0.08189992,
            0.20469298,
            0.10205455,
            0.17487756,
        ],
        [
            0.25657813,
            0.19143197,
            0.24972915,
            0.17095683,
            0.27081193,
            0.2462393,
            0.21611095,
            0.08189992,
            1.00001877,
            0.18194598,
            0.17319142,
            0.25291604,
        ],
        [
            0.24761691,
            0.16329805,
            0.18741918,
            0.10991068,
            0.25982165,
            0.13388592,
            0.16251798,
            0.20469298,
            0.18194598,
            1.00001877,
            0.11709855,
            0.24620818,
        ],
        [
            0.1748068,
            0.18374061,
            0.20326165,
            0.13512469,
            0.15926039,
            0.22042753,
            0.23562067,
            0.10205455,
            0.17319142,
            0.11709855,
            1.00001877,
            0.17115672,
        ],
        [
            0.33267418,
            0.19766977,
            0.25492511,
            0.12871365,
            0.31415826,
            0.17894845,
            0.20001099,
            0.17487756,
            0.25291604,
            0.24620818,
            0.17115672,
            1.00001877,
        ],
    ]
)


def component_loadings():
    return pd.DataFrame(
        data=ROTATED_PCA_LOADINGS,
        index=test_names(exclude=["sar"]),
        columns=DOMAIN_NAMES,
    )


def covariance():
    return pd.DataFrame(
        data=COVARIANCE,
        index=test_names(exclude=["sar"]),
        columns=test_names(exclude=["sar"]),
    )
