import pytest
import os.path as path
import hashlib
import pandas as pd
from cbspython import (
    MeaningfulChangeModel as MCM, hc_feature_list, ArgumentValueError
)
import numpy as np
from numpy import NAN
from contextlib import nullcontext as does_not_raise

all_hc_features = hc_feature_list(abbreviated=True)
mcm = MCM()


def test_MCM_instance_class():
    assert isinstance(mcm, MCM)


def test_src_data_exists():
    assert path.exists(MCM.default_src_file())


def test_src_data_MD5():
    expected_MD5 = "80e13c145bc05cd51af73140a64be277"
    source_MD5 = hashlib.md5()
    with open(MCM.default_src_file(), "rb") as f:
        for byte_block in iter(lambda: f.read(4096), b""):
            source_MD5.update(byte_block)
    assert source_MD5.hexdigest() == expected_MD5


def test_internal_data_shape():
    assert mcm._data.shape == (20, 24)


@pytest.mark.parametrize(
    "arguments, expectation",
    [
        ({}, does_not_raise()),
        ({'not_an_index': []}, pytest.raises(ArgumentValueError)),
        ({'baseline': ['1st', 'n-1']}, does_not_raise()),
        ({'baseline': 'not_a_baseline'}, pytest.raises(ArgumentValueError)),
        ({'test': all_hc_features}, does_not_raise()),
        ({'test': 'not_a_test'}, pytest.raises(ArgumentValueError)),
        ({'bound': ['low', 'high']}, does_not_raise()),
        ({'bound': 'not_low_or_high'}, pytest.raises(ArgumentValueError)),
        ({'time_point': np.arange(0, 10)+1}, does_not_raise()),
        ({'time_point': [11]}, pytest.raises(ArgumentValueError)),
    ]
)
def test_internal_data_index(arguments, expectation):
    with expectation:
        mcm.get_thresholds(**arguments)


def test_invalid_direction():
    with pytest.raises(ArgumentValueError):
        mcm.analyze(make_df(), '1st', direction='invalid_option')


def make_df(index=None, columns=None, data=None):

    def index_from(opts):
        levels = [np.arange(i)+1 if isinstance(i, int) else i for
                  i in opts.values()]
        names = list(opts.keys())
        if len(names) > 1:
            index = pd.MultiIndex.from_product(levels, names=opts.keys())
        else:
            index = pd.Index(levels[0], name=names[0])
        return index

    if data is not None:
        data = np.atleast_2d(data)

    if index is None:
        nr = 3 if data is None else data.shape[0]
        index = {'user': 1, 'time_point': nr}

    if columns is None:
        nc = 2 if data is None else data.shape[1]
        assert nc <= len(all_hc_features)
        columns = {'test': all_hc_features[0:nc]}

    index = index_from(index)
    columns = index_from(columns)

    return pd.DataFrame(data, index=index,  columns=columns)


@pytest.fixture
def basic_df():
    return make_df()


@pytest.mark.parametrize(
    "baseline_type_str, expectation",
    [
        ('bad_type', pytest.raises(ArgumentValueError)),
        ('1st', does_not_raise()),
        ('n-1', does_not_raise()),
    ],
)
def test_baseline_types(baseline_type_str, expectation, basic_df):
    with expectation:
        mcm._baseline_df(basic_df, baseline_type_str)


@pytest.mark.parametrize(
    "desc, df_params, expectation",
    [
        ("bad_index", {'index': {'user': 2}}, pytest.raises(
                Exception, match=r"Index requires a 'time_point' level."
            )),
        ("bad_index", {'index': {'time_point': 10}}, pytest.raises(
                Exception, match=r"Index requires a 'user' level."
            )),
        ("bad_column_index", {'columns': {1: 2, 2: 2, 3: 2}}, pytest.raises(
                Exception, match=r"Too many levels in column MultiIndex."
            )),
        ("bad_column_names", {'columns': {'test': ['not_a_valid_score']}},
            pytest.raises(
                Exception,
                match=r"No columns in dataframe have HC feature names."
            )),
        ("bad_users", {'index': {'user': 2, 'time_point': 1}},
            pytest.raises(
                Exception,
                match=r"Users \[1, 2\] have fewer than 2 data points in"
            )),
        ("valid_df", {}, does_not_raise()),
    ],
)
def test_input_df_validation(desc, df_params, expectation):
    df = make_df(**df_params)
    with expectation:
        mcm._validate_input_dataframe(df)


X = [
    [0, 3],
    [1, 6],
    [2, 12]
]


def make_matrix_of_MC_scores(baseline, bound):
    return (
        mcm.get_thresholds(baseline=baseline, bound=bound)
        .apply(lambda x: x+{'high': 0.01, 'low': -0.01}[bound])  # Add offset
        .pipe(lambda x: x.cumsum(axis=0) if baseline == 'n-1' else x)
        .fillna(0)                                  # Fill first row with 0
        .droplevel('bound', axis=1)                 # Drop empty index level
        .loc[:, all_hc_features]                    # Re-order columns to match
        .rename_axis(index={'baseline': 'user'})    # Fake a user column
        .values
    )


def make_MC_results(result):
    x = np.full([mcm._num_dt, mcm._num_tests], result)
    if result:
        x[0, :] = False  # First row can't be True...
    return x


@pytest.mark.parametrize(
    "function, args, kwargs, input_data, expected_data",
    [
        ('_validate_input_dataframe', [], {}, X, X),
        ('_baseline_df', ['1st'], {}, X, [[NAN, NAN], [0, 3], [0, 3]]),
        ('_baseline_df', ['n-1'], {}, X, [[NAN, NAN], [0, 3], [1, 6]]),
        ('_delta_df', ['1st'], {}, X, [[NAN, NAN], [1, 3], [2, 9]]),
        ('_delta_df', ['n-1'], {}, X, [[NAN, NAN], [1, 3], [1, 6]]),

        ('analyze', ['1st'], {'direction': 'increase'},
            make_matrix_of_MC_scores('1st', 'high'), make_MC_results(True)),

        ('analyze', ['1st'], {'direction': 'decrease'},
            make_matrix_of_MC_scores('1st', 'low'), make_MC_results(True)),

        ('analyze', ['n-1'], {'direction': 'increase'},
            make_matrix_of_MC_scores('n-1', 'high'), make_MC_results(True)),

        ('analyze', ['n-1'], {'direction': 'decrease'},
            make_matrix_of_MC_scores('n-1', 'low'), make_MC_results(True)),
    ],
)
def test_expected_data(function, args, kwargs, input_data, expected_data):
    input_df = make_df(data=input_data)
    output_df = getattr(mcm, function)(input_df, *args, **kwargs)
    expected_df = make_df(data=expected_data)
    pd.testing.assert_frame_equal(output_df, expected_df)
