import pytest
import numpy as np
import pandas as pd

from contextlib import nullcontext as does_not_raise
from numpy.testing import (
    assert_array_almost_equal, assert_array_equal, assert_almost_equal
)
from pytest_cases import parametrize_with_cases, parametrize, fixture

from cbspython import (
    TESTS, DataTypeError, DataSizeError, standard_parameters,
    to_z, z_to_score, z_to_p, p_to_z, p_to_rank, to_adjusted,
    to_standard_score, standard_error_of_estimation, confidence_intervals
)

from ..cbsscores import (
    _good_data_types, _validate_inputs
)


def test_good_datatypes():
    assert _good_data_types() == (
        pd.DataFrame, pd.Series, np.ndarray, np.number
    )


@fixture
def std_params():
    return {'mean': 100, 'std': 15}


def test_standard_parameters(std_params):
    assert standard_parameters() == std_params


@parametrize(
    "data", [
        pd.DataFrame([], dtype='float32'),  # we can work with dataframes,
        pd.Series([], dtype='float32'),     # or series,
        np.array([]),                       # numpy arrays are great,
        42,                                 # numbers will converted to np,
        42.2
    ]
)
def case_data_type_pass(data):
    return {'pass': data}, does_not_raise()


@parametrize(
    "data", [
        [], 'string', (), None
    ]
)
def case_data_type_fail(data):
    return {'fail': data}, pytest.raises(DataTypeError)


@parametrize(
    "shape1, shape2", [
        ([2], [3]),
        ([1, 2], [3, 4]),
        ([10, 2], 15)
    ]
)
def case_data_size_fail(shape1, shape2):
    return (
        {'shape1': np.zeros(shape1), 'wrongshape2': np.zeros(shape2)},
        pytest.raises(DataSizeError)
    )


@parametrize(
    "shape1, shape2", [
        ([2, 1], [1, 2]),
        ([4, 1], [4, 2]),
        (4, [2, 4]),
        (5, 5),
        ([3, 2], [3, 2])
    ]
)
def case_data_size_pass(shape1, shape2):
    return (
        {'shape1': np.zeros(shape1), 'correctshape2': np.zeros(shape2)},
        does_not_raise()
    )


@parametrize_with_cases("data, expectation", cases='.', prefix='case_data_')
def test_validate_inputs(data, expectation):
    with expectation:
        _validate_inputs(**data)


@fixture
def pop_params():
    return {
        'mean': 10,
        'std': 2.5,
    }


@fixture
def data_raw():
    return np.array([5, 7.5, 10, 12.5, 15])


@fixture
def data_std():
    return np.array([70, 85, 100, 115, 130])


@fixture
def data_z():
    return np.array([-2, -1, 0, 1, 2])


@fixture
def data_p():
    return np.array([0.02275, 0.158655, 0.5, 0.841345, 0.97725])


@fixture
def data_ranks():
    return np.array([20, 17, 10, 4, 1])


def test_to_z(data_raw, pop_params, data_z):
    zs = to_z(data_raw, pop_params['mean'], pop_params['std'])
    assert_array_almost_equal(data_z, zs)


def test_to_std(data_raw, pop_params, data_std):
    stds = to_standard_score(data_raw, pop_params['mean'], pop_params['std'])
    assert_array_equal(stds, data_std)


def test_z_to_score(data_z, pop_params, data_raw):
    raw_scores = z_to_score(data_z, pop_params['mean'], pop_params['std'])
    assert_array_almost_equal(raw_scores, data_raw, decimal=6)


def test_z_to_p(data_z, data_p):
    assert_array_almost_equal(z_to_p(data_z), data_p, decimal=4)


def test_p_to_z(data_p, data_z):
    assert_array_almost_equal(p_to_z(data_p), data_z, decimal=4)


def test_p_to_rank(data_p, data_ranks):
    assert_array_equal(p_to_rank(data_p), data_ranks)


def case_r_vhigh(data_raw):
    """ Perfect reliability (1.0) means that the resulting adjusted scores
        should be exactly the input scores. """
    return (
        1.0,            # perfect reliability
        data_raw        # means the results scores are exactly the input
    )


def case_r_vlow(pop_params, data_raw):
    """ Zero reliability means that the resulting adjusted scores
        should be the mean of the sample. """
    return (
        0.0,
        np.repeat(pop_params['mean'], data_raw.shape[0])
    )


@parametrize_with_cases(
    "reliability, expected_scores", cases='.', prefix='case_r'
)
def test_to_adjusted(data_raw, pop_params, reliability, expected_scores):
    adjusted = to_adjusted(data_raw, pop_params['mean'], reliability)
    assert_array_almost_equal(adjusted, expected_scores)


@parametrize(
    "test_name, expected_sem",
    [
        ("double_trouble", 5.728924527690603),
        ("odd_one_out", 7.468375223540928),
        ("spatial_planning", 6.6468954993872895),
        ("grammatical_reasoning", 6.4212760576052945),
        ("digit_span", 7.321640299270376),
        ("token_search", 7.358819493852257),
        ("paired_associates", 7.498041442130071),
        ("spatial_span", 7.337845747341314),
        ("feature_match", 7.4432914559017505),
        ("rotations", 7.347226557986014),
        ("polygons", 7.491629597250521),
        ("monkey_ladder", 7.392764966490887),
    ],
)
def test_standard_error_of_estimation(test_name, expected_sem):
    std = standard_parameters()['std']
    r = TESTS[test_name].reliability
    calculated_sem = standard_error_of_estimation(std, r)
    assert_almost_equal(calculated_sem, expected_sem)


@parametrize(
    "test_name, score, expected_CIs",
    [
        ("double_trouble", 71.0915433, [64.9887293, 87.4457008]),
        ("odd_one_out", 85.5909482, [78.818642, 108.0941349]),
        ("spatial_planning", 85.4915299, [76.357913, 102.4132646]),
        ("grammatical_reasoning", 78.1242438, [70.8251161, 95.9960558]),
        ("digit_span", 98.8610114, [84.9568948, 113.6571974]),
        ("token_search", 57.7350625, [60.3634922, 89.2095346]),
        ("paired_associates", 85.4999417, [78.2197573, 107.6115397]),
        ("spatial_span", 90.8703878, [80.1091965, 108.8730232]),
        ("feature_match", 94.8903159, [82.5429935, 111.7201598]),
        ("rotations", 65.9261758, [65.1416062, 93.942205]),
        ("polygons", 104.1762922, [87.5034492, 116.8700976]),
        ("monkey_ladder", 94.0558065, [82.0375578, 111.016664]),
    ]
)
def test_confidence_intervals(test_name, score, expected_CIs, std_params):
    std = std_params['std']
    mean = std_params['mean']
    r = TESTS[test_name].reliability
    calculated_CIs = confidence_intervals(score, mean, std, r, width=0.95)
    assert_almost_equal(calculated_CIs, expected_CIs, decimal=7)
