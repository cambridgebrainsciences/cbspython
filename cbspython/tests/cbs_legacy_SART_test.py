# Testing cbstests.py
from cbspython.cbstests import (
    SARTest
)

import pytest
import os.path as path
import pandas as pd


@pytest.fixture
def sar():
    return SARTest()


@pytest.mark.parametrize(
    "feature, field",
    [
        ('final_score', 'single_score'),
        ('nogo_success', 'raw_field_01'),
        ('go_success', 'raw_field_02'),
        ('num_commissions', 'raw_field_03'),
        ('pct_commissions', 'raw_field_04'),
        ('num_omissions', 'raw_field_05'),
        ('pct_omissions', 'raw_field_06'),
        ('num_anticipatory', 'raw_field_07'),
        ('num_ambiguous', 'raw_field_08'),
        ('avg_RT_go', 'raw_field_09'),
        ('std_RT_go', 'raw_field_10'),
        ('RT_cv', 'raw_field_11'),
        ('avg_RT_b4_nogo_success', 'raw_field_12'),
        ('avg_RT_b4_nogo_fail', 'raw_field_13'),
    ]
)
def test_feature_map(feature, field, sar):
    assert sar.field_for_feature(feature) == field


@pytest.fixture
def sar_test_data():
    return pd.read_csv(
        path.join(
            path.dirname(__file__),
            "cbs_SART_legacy_data.csv"
        )
    )


@pytest.fixture
def sar_raw_str(sar_test_data):
    return sar_test_data.loc[1, 'Legacy Raw Score']


@pytest.fixture
def sar_session_data(sar_test_data):
    return sar_test_data.loc[1, 'Session Data']


def test_sar_parse_legacy_raw(sar, sar_raw_str):
    actual = sar.parse_raw(sar_raw_str)
    assert actual == {
        'go_success': 108.0,
        'num_commissions': 22.0,
        'pct_commissions': 88.0,
        'num_omissions': 6.0,
        'pct_omissions': 3.0,
        'num_anticipatory': 52.0,
        'num_ambiguous': 37.0,
        'avg_RT_go': 305.655,
        'std_RT_go': 306.049,
        'RT_cv': 0.999,
        'avg_RT_b4_nogo_success': 0.0,
        'avg_RT_b4_nogo_fail': 439.55,
    }


def test_sar_parse_session(sar, sar_session_data):
    session_features = sar.parse_session(sar_session_data)
    calculated_features = sar.calculate_features(session_features)

    assert calculated_features == {
        'nogo_success': 5,
        'post_CE_slowing': -695
    }
