# Testing the Report class in cbsreport.py

from unittest.mock import MagicMock
import pytest
import os.path as path
from cbspython import Report, ReportParser
from cbspython.cbsreport import DeviceType, parse_score_features, Locale
from contextlib import nullcontext as does_not_raise

# 1. Test if the class can be imported from this package
# (i.e., is available externally)


def test_report_class_import():
    try:
        from cbspython import Report  # noqa: F401
    except ImportError:
        pytest.fail("Error: can not import the Report class from cbspython")


# 2. Give error message if a source filename or data structure is not passed
# as input to constructor


class InvalidStringException(Exception):
    pass


class InvalidSrcTypeException(Exception):
    pass


testdata_dir = path.dirname(__file__)


@pytest.mark.parametrize(
    "source, exception",
    [
        (
            "stringfortesting.txt",
            TypeError("Not sure how to handle stringfortesting.txt"),
        ),
        (None, TypeError(f"Invalid src type {None.__class__}")),
    ],
)
def test_no_src_passed(source, exception):
    try:
        ReportParser(source, n_jobs=-1)
    except TypeError as inst:
        # First ensure the exception is of the right type
        assert isinstance(inst, type(exception))
        # Ensure that exceptions have the same message
        assert inst.args == exception.args
    else:
        pytest.fail("Expected error but found none")


# 3. Test if it can successfully return a Report object
# when called with Test Dataset A


@pytest.fixture
def report():
    return Report.from_raw_data(
        path.join(testdata_dir, "cbs_example_data_A.csv"),
        n_jobs=-1
    )


@pytest.fixture
def sart_report():
    return Report.from_raw_data(
        path.join(testdata_dir, "cbs_SART_legacy_data.csv"),
        n_jobs=1
    )


# 4. Test if the returned Report object has the correct dataframe shape,
# trial name and number of subjects when called with Test Dataset A

def test_if_SART_Report_object_has_correct_shape(sart_report):
    assert sart_report.data.shape == (5, 40)


def test_if_Report_object_has_correct_shape(report):
    assert report.data.shape == (24, 40)


def test_if_Report_object_has_correct_trial_name(report):
    assert report.trial_names == 'test_data'


def test_if_Report_object_has_correct_number_of_subjects(report):
    assert report.num_subjects == 2


def test_if_Report_can_load_from_pickle():
    with does_not_raise():
        Report.from_pickle(
            path.join(testdata_dir, 'cbs_example_data_A.report.pickle')
        )

# 5. Tests for non-class functions


def test_parse_score_features_invalid_score_format_raw_score(mocker):
    df = MagicMock()
    df.columns = {"session_data": 1}
    df.shape[0] = 2

    mocked_test = MagicMock()
    mocked_test.parse_raw = MagicMock(side_effect=Exception(
                                                        "Something happened."))
    mocked_test.parse_session = MagicMock(side_effect=Exception(
                                                        "Something happened."))
    mocked_TESTS = {"spatial_span": mocked_test}
    mocked_row = {"raw_score": 5, "id": 6, "session_data": 1}

    idx = 0
    mocker.patch(
        "cbspython.cbsreport.enumerate",
        return_value=[[3, ((idx, "spatial_span"), mocked_row)]],
    )
    mocker.patch("cbspython.cbsreport.TESTS", mocked_TESTS)
    mocked_print = mocker.patch("cbspython.cbsreport.print", return_value=None)

    parse_score_features(df)

    assert (
        mocked_print.call_args_list[1][0][0]
        == ("WARNING: Invalid raw score format for row 6"
            " (0, 'spatial_span') with data (5)")
    )


def test_parse_score_features_invalid_score_format_session_data(mocker):
    df = MagicMock()
    df.columns = {"session_data": 1}
    df.shape[0] = 2

    mocked_test = MagicMock()
    mocked_test.parse_raw = MagicMock(side_effect=Exception(
                                                        "Something happened."))
    mocked_test.parse_session = MagicMock(side_effect=Exception(
                                                    "Something happened."))
    mocked_TESTS = {"spatial_span": mocked_test}
    mocked_row = {"raw_score": 5, "id": 6, "session_data": 1}

    idx = 0
    mocker.patch(
        "cbspython.cbsreport.enumerate",
        return_value=[[3, ((idx, "spatial_span"), mocked_row)]],
    )
    mocker.patch("cbspython.cbsreport.TESTS", mocked_TESTS)
    mocked_print = mocker.patch("cbspython.cbsreport.print", return_value=None)

    parse_score_features(df)

    assert (
        mocked_print.call_args_list[2][0][0]
        == ("WARNING: Invalid session_data for row 6"
            " (0, 'spatial_span') with data (1)")
    )


# 6. Tests for the Enum class


def test_DeviceType_Enum_codes():
    expected = [0, 1, 2, 3, 4, 5]
    actual = DeviceType.codes()

    assert actual == expected


def test_DeviceType_Enum_labels():
    expected = ["BOT", "TV", "CONSOLE", "TABLET", "MOBILE", "DESKTOP"]
    actual = DeviceType.labels()

    assert actual == expected


def test_Locale_Enum_codes():
    expected = [0, 1, 2, 3, 4]
    actual = Locale.codes()

    assert actual == expected


def test_Locale_Enum_labels():
    expected = ["EN", "FR", "NL", "DE", "ES"]
    actual = Locale.labels()

    assert actual == expected
