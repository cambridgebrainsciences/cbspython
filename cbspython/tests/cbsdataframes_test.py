import pytest
from typing import Union
from cbspython import unabbreviate_feature


@pytest.mark.parametrize(
    "input, expected",
    [
        ("DT_final_score", ("double_trouble", "final_score")),
        ("GR_final_score", ("grammatical_reasoning", "final_score")),
        ("PO_final_score", ("polygons", "final_score")),
        ("FM_final_score", ("feature_match", "final_score")),
        ("RT_final_score", ("rotations", "final_score")),
        ("OOO_final_score", ("odd_one_out", "final_score")),
        ("SP_final_score", ("spatial_planning", "final_score")),
        ("SS_avg_score", ("spatial_span", "avg_score")),
        ("DS_avg_score", ("digit_span", "avg_score")),
        ("ML_avg_score", ("monkey_ladder", "avg_score")),
        ("PA_avg_score", ("paired_associates", "avg_score")),
        ("TS_avg_score", ("token_search", "avg_score")),
        ("SAR_num_commissions", ("sar", "num_commissions")),
        (("double_trouble", "final_score"), ("double_trouble", "final_score")),
    ],
)
def test_unabbreviate_feature(
    input: Union[str, tuple[str, str]], expected: tuple[str, str]
):
    assert unabbreviate_feature(input) == expected
