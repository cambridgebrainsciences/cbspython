# Testing cbstests.py
import json
from unittest.mock import MagicMock
from cbspython.cbstests import (
    TESTS, test_names, Test, MemoryTest, SpatialSpanTest, DigitSpanTest,
    MonkeyLadderTest, PairedAssociatesTest, TokenSearchTest, RotationsTest,
    FeatureMatchTest, OddOneOutTest, DoubleTroubleTest, PolygonsTest,
    SpatialPlanningTest, GrammaticalReasoningTest,
    timing_features, abbrev_features, abbrev_columns,
    test_features, domain_feature_list, hc_feature_list, tests_of_type,
    memory_tests, reasoning_tests, set_column_names, abbreviate_columns,
    component_loadings, covariance
)
from cbspython.tests.test_data import (
    FEATURES_TO_EXCLUDE_V3, EXPECTED_FEATURES_V7,
    EXPECTED_FEATURES_V8, EXPECTED_ABBREVIATED_FEATURES_V1,
    EXPECTED_ABBREVIATED_FEATURES_V2, EXPECTED_FEATURES_V11,
    EXPECTED_HC_FEATURES
)
import pytest
import os.path as path
import numpy as np
import pandas as pd

from numpy.testing import assert_almost_equal
from pytest_cases import parametrize_with_cases


test_data = pd.read_csv(
    path.join(
        path.dirname(__file__),
        'cbs_example_data_A.csv'
    )
).set_index("Test Name")

# 1. Test if TESTS is available to be imported


def test_TESTS_import():
    try:
        from cbspython import TESTS  # noqa: F401
    except ImportError:
        pytest.fail("Error: can not import TESTS from cbspython")


# 2. Test if TESTS a 12-item dictionary


def test_if_TESTS_is_13_item_dict():
    assert isinstance(TESTS, dict)
    assert len(TESTS) == 13


# 3. Test if the keys of TESTS dict correspond to the expected name
# of the tasks (e.g., digit_span, double_trouble, etc.)

@pytest.fixture
def all_test_names():
    return [
        "spatial_span",
        "grammatical_reasoning",
        "double_trouble",
        "odd_one_out",
        "monkey_ladder",
        "rotations",
        "feature_match",
        "digit_span",
        "spatial_planning",
        "paired_associates",
        "polygons",
        "token_search",
        "sar"
    ]


def test_if_TESTS_keys_are_correct(all_test_names):
    assert list(TESTS.keys()) == all_test_names


def test_test_names(all_test_names):
    assert test_names() == all_test_names


# 4. Test if the corresponding values of the dict are Test objects


def test_if_TESTS_values_are_Test_objects():
    for values in TESTS.values():
        assert isinstance(values, Test)


# 5. For the Test class

# Test getters for properties


def test_name_getter():
    assert Test().name == ""


def test_id_getter():
    assert Test().id is None


def test_reliability_getter():
    assert Test().reliability == 0


def test_feature_fields_getter():
    expected = ["single_score"]
    t = Test()

    actual = t.feature_fields

    assert expected == actual


def test_hc_feature_getter():
    expected = "final_score"
    t = Test()

    actual = t.hc_feature

    assert expected == actual


def test_hc_field_getter():
    expected = "single_score"
    t = Test()

    actual = t.hc_field

    assert expected == actual


def test_domain_feature_getter():
    expected = "final_score"
    t = Test()

    actual = t.domain_feature

    assert expected == actual


def test_domain_field_getter():
    expected = "single_score"
    t = Test()

    actual = t.domain_field

    assert expected == actual


def test_all_session_features_getter():
    t = Test()
    expected = list(t.all_session_feature_calcs.keys())

    actual = t.all_session_features

    assert expected == actual


# Test methods


def test_num_features_when_include_common_true():

    expected = 9
    t = Test()

    actual = t.num_features(include_common=True)

    assert expected == actual


def test_num_features_when_include_common_false():

    expected = 1
    t = Test()

    actual = t.num_features(include_common=False)

    assert expected == actual


# case 1: invalid json string


def test_parse_session_invalid_json(mocker):

    session_data_str = "A"
    expected = {}
    expected_msg = "WARNING: Cannot parse JSON session A"
    mocked_print = mocker.patch("cbspython.cbstests.print", return_value=None)
    t = Test()

    actual = t.parse_session(session_data_str)

    assert expected == actual
    mocked_print.assert_called_once_with(expected_msg)


# case 2: valid json string, and attr at 'code' != ''


def test_parse_session_valid_json_and_abbrv(mocker):

    session_data_str = '{"code": "a"}'
    expected = {}
    expected_msg = "WARNING: Invalid Task Code, got a expected "
    mocked_print = mocker.patch("cbspython.cbstests.print", return_value=None)
    t = Test()

    actual = t.parse_session(session_data_str)

    assert expected == actual
    mocked_print.assert_called_once_with(expected_msg)


# case 3: valid json string and attr at 'code' == ''
# and extract_features = False


def test_parse_session_valid_json_and_abbrv_without_extract_features():

    session_data_str = '{"code": ""}'
    expected = json.loads(session_data_str)
    t = Test()

    actual = t.parse_session(session_data_str, extract_features=False)

    assert expected == actual


def test_question_difficulty_nan():

    session_data_str = '{"code": ""}'
    sessionData = json.loads(session_data_str)

    actual = Test().question_difficulty(sessionData)

    assert np.isnan(actual)


def test_extract_trial_data_none():
    sessionData = None
    expected = pd.DataFrame(
        [], columns=["correct", "difficulty", "duration"]
    ).rename_axis("trial")

    actual = Test().extract_trial_data(sessionData)

    assert expected.equals(actual)


@pytest.mark.parametrize(
    "session_data_str, expected",
    [
        (
            '{"code": ""}',
            pd.DataFrame(
                [], columns=["correct", "difficulty", "duration"]
                ).rename_axis(
                "trial"
            ),
        ),
        (
            '{"question": ""}',
            pd.DataFrame(
                [], columns=["correct", "difficulty", "duration"]
                ).rename_axis(
                "trial"
            ),
        ),
        (
            (
                '{"questions":[{"endTimeSpan": 12, "isCorrect": true, '
                '"durationTimeSpan": 123}, {"endTimeSpan": 12, '
                '"isCorrect": false, "durationTimeSpan": 345}]}'
            ),
            pd.DataFrame(
                [[True, np.nan, 123], [False, np.nan, 345]],
                columns=["correct", "difficulty", "duration"],
            ).rename_axis("trial"),
        ),
    ],
)
def test_extract_trial_data_not_none(session_data_str, expected):

    sessionData = json.loads(session_data_str)

    actual = Test().extract_trial_data(sessionData)

    assert expected.equals(actual)


# 6. For each of the 12 tests, test if the id, abbrev, rhc, hc feature
# and domain feature is correct

# Memory Test


def test_parse_raw_memory_test():
    expected = {"avg_score": 0}
    t = MemoryTest()

    actual = t.parse_raw("")

    assert expected == actual


def test_SpatialSpanTest():
    assert SpatialSpanTest._id == 20
    assert SpatialSpanTest._abbrev == "SS"
    assert SpatialSpanTest._r_hc == 0.603409
    assert SpatialSpanTest._hc_feature == "avg_score"
    assert SpatialSpanTest._domain_feature == "max_score"


def test_parse_raw_test_SpatialSpanTest():
    raw_score_str = test_data['Legacy Raw Score']['Spatial Span'][0]
    t = SpatialSpanTest()
    expected = {'avg_score': 4.75}

    actual = t.parse_raw(raw_score_str)

    assert actual == expected


def test_SS_parse_session():
    t = SpatialSpanTest()
    expected = {
        'num_errors': 3,
        'num_correct': 4,
        'num_attempts': 7,
        'accuracy': 0.5714,
        'better_than_chance': True,
        'duration_ms': 81003,
        'avg_ms_correct': 8901.0,
        'avg_ms_per_item': 1877.0875,
        'std_ms_correct': 821.7490
    }

    actual = t.parse_session(
        session_data_str=test_data['Session Data']['Spatial Span'][0],
        extract_features=True
    )
    for f, v in expected.items():
        assert_almost_equal(v, actual[f], err_msg=f, decimal=2)


def test_question_difficulty_SS():

    q = {"question": [1, 5]}
    expected = 2
    t = SpatialSpanTest()

    actual = t.question_difficulty(q)

    assert expected == actual


def test_DigitSpanTest():
    assert DigitSpanTest._id == 17
    assert DigitSpanTest._abbrev == "DS"
    assert DigitSpanTest._r_hc == 0.608394
    assert DigitSpanTest._hc_feature == "avg_score"
    assert DigitSpanTest._domain_feature == "max_score"


def test_DS_parse_session():
    t = DigitSpanTest()
    expected = {
        'num_errors': 3,
        'num_correct': 10,
        'num_attempts': 13,
        'accuracy': 0.76923,
        'better_than_chance': True,
        'duration_ms': 315166,
        'avg_ms_correct': 15371.0,
        'avg_ms_per_item': 2181.1813,
        'std_ms_correct': 5228.593577
    }

    actual = t.parse_session(
        session_data_str=test_data['Session Data']['Digit Span'][0],
        extract_features=True
    )

    for f, v in actual.items():
        assert_almost_equal(v, expected[f], err_msg=f, decimal=2)


def test_question_difficulty_DS():

    q = {"attributes": {"question": "7612", "answer": "7612", "score": 4}}
    expected = 4
    t = DigitSpanTest()

    actual = t.question_difficulty(q)

    assert expected == actual


def test_MonkeyLadderTest():
    assert MonkeyLadderTest._id == 24
    assert MonkeyLadderTest._abbrev == "ML"
    assert MonkeyLadderTest._r_hc == 0.584249
    assert MonkeyLadderTest._hc_feature == "avg_score"
    assert MonkeyLadderTest._domain_feature == "max_score"


def test_ML_parse_session():
    t = MonkeyLadderTest()
    expected = {
        'num_errors': 3,
        'num_correct': 8,
        'num_attempts': 11,
        'accuracy': 0.727272,
        'better_than_chance': True,
        'duration_ms': 91950,
        'avg_ms_correct': 6491.875,
        'avg_ms_per_item': 1559.171428,
        'std_ms_correct':  2924.42680
    }

    actual = t.parse_session(
        session_data_str=test_data['Session Data']['Monkey Ladder'][0],
        extract_features=True
    )

    for f, v in actual.items():
        assert_almost_equal(v, expected[f], err_msg=f, decimal=2)


def test_question_difficulty_ML():

    q = {"question": [1, 5]}
    expected = 2
    t = MonkeyLadderTest()

    actual = t.question_difficulty(q)

    assert expected == actual


def test_PairedAssociatesTest():
    assert PairedAssociatesTest._id == 19
    assert PairedAssociatesTest._abbrev == "PA"
    assert PairedAssociatesTest._r_hc == 0.488574
    assert PairedAssociatesTest._hc_feature == "avg_score"
    assert PairedAssociatesTest._domain_feature == "max_score"


def test_PA_parse_session():
    t = PairedAssociatesTest()
    expected = {
        'num_errors': 3,
        'num_correct': 5,
        'num_attempts': 8,
        'accuracy': 0.625,
        'better_than_chance': True,
        'duration_ms': 107664,
        'avg_ms_correct': 11429.4,
        'avg_ms_per_item': 4494.55,
        'std_ms_correct':  2684.03663
    }

    actual = t.parse_session(
        session_data_str=test_data['Session Data']['Paired Associates'][0],
        extract_features=True
    )

    for f, v in actual.items():
        assert_almost_equal(v, expected[f], err_msg=f, decimal=2)


def test_question_difficulty_PA():

    q = {"question": [1, 5]}
    expected = 2
    t = PairedAssociatesTest()

    actual = t.question_difficulty(q)

    assert expected == actual


def test_TokenSearchTest():
    assert TokenSearchTest._id == 18
    assert TokenSearchTest._abbrev == "TS"
    assert TokenSearchTest._r_hc == 0.596558
    assert TokenSearchTest._hc_feature == "avg_score"
    assert TokenSearchTest._domain_feature == "max_score"


def test_TS_parse_session():
    t = TokenSearchTest()
    expected = {
        'num_errors': 3,
        'num_correct': 3,
        'num_attempts': 6,
        'accuracy': 0.5,
        'better_than_chance': True,
        'duration_ms': 81422,
        'avg_ms_correct': 14428.333,
        'avg_ms_per_item': 2973.450,
        'std_ms_correct': 313.09352
    }

    actual = t.parse_session(
        session_data_str=test_data['Session Data']['Token Search'][0],
        extract_features=True
    )

    for f, v in actual.items():
        assert_almost_equal(v, expected[f], err_msg=f, decimal=2)


def test_question_difficulty_TS():

    q = {
        "attributes": {"0": {
            "foundTokenNum": 0, "tokenIndex": 9, "difficultyLevel": 7
            }}
    }
    expected = 7
    t = TokenSearchTest()

    actual = t.question_difficulty(q)

    assert expected == actual


# Reasoning Test


def test_RotationsTest():
    assert RotationsTest._id == 22
    assert RotationsTest._abbrev == "RT"
    assert RotationsTest._r_hc == 0.600405
    assert RotationsTest._hc_feature == "final_score"
    assert RotationsTest._domain_feature == "final_score"


def test_parse_raw_RotationsTest():
    raw_score_str = test_data['Legacy Raw Score']['Rotations'][0]
    t = RotationsTest()
    expected = {
        'attempted': 14.0,
        'errors': 3.0,
        'max': 9.0,
        'score': 50.0,
        'correct_score': 74.0
    }

    actual = t.parse_raw(raw_score_str)

    assert actual == expected


def test_RT_parse_session():
    t = RotationsTest()
    expected = {
        'num_errors': 3,
        'num_correct': 11,
        'num_attempts': 14,
        'accuracy': 0.78571,
        'better_than_chance': True,
        'duration_ms': 90028,
        'avg_ms_correct': 5953.6363,
        'std_ms_correct': 2426.98351
    }

    actual = t.parse_session(
        session_data_str=test_data['Session Data']['Rotations'][0],
        extract_features=True
    )

    for f, v in actual.items():
        assert_almost_equal(v, expected[f], err_msg=f, decimal=2)


def test_question_difficulty_RT():

    q = {"difficulty": 3}
    expected = 3
    t = RotationsTest()

    actual = t.question_difficulty(q)

    assert expected == actual


def test_FeatureMatchTest():
    assert FeatureMatchTest._id == 21
    assert FeatureMatchTest._abbrev == "FM"
    assert FeatureMatchTest._r_hc == 0.56137
    assert FeatureMatchTest._hc_feature == "final_score"
    assert FeatureMatchTest._domain_feature == "final_score"


def test_parse_raw_FeatureMatchTest():
    raw_score_str = test_data['Legacy Raw Score']["Feature Match"][0]
    t = FeatureMatchTest()
    expected = {
        'attempted': 21.0,
        'errors': 1.0,
        'max': 10.0,
        'score': 100.0,
        'correct_score': 111.0
    }

    actual = t.parse_raw(raw_score_str)

    assert actual == expected


def test_FM_parse_session():
    t = FeatureMatchTest()
    expected = {
        'num_errors': 1,
        'num_correct': 20,
        'num_attempts': 21,
        'accuracy': 0.9523,
        'better_than_chance': True,
        'duration_ms': 90012,
        'avg_ms_correct': 3385.2,
        'std_ms_correct': 2077.97198
    }

    actual = t.parse_session(
        session_data_str=test_data['Session Data']['Feature Match'][0],
        extract_features=True
    )

    for f, v in actual.items():
        assert_almost_equal(v, expected[f], err_msg=f, decimal=2)


def test_question_difficulty_FM():

    q = {"difficulty": 3}
    expected = 3
    t = FeatureMatchTest()

    actual = t.question_difficulty(q)

    assert expected == actual


def test_OddOneOutTest():
    assert OddOneOutTest._id == 14
    assert OddOneOutTest._abbrev == "OOO"
    assert OddOneOutTest._r_hc == 0.454132
    assert OddOneOutTest._hc_feature == "final_score"
    assert OddOneOutTest._domain_feature == "max"


def test_parse_raw_OddOneOutTest():
    raw_score_str = test_data['Legacy Raw Score']["Odd One Out"][0]
    t = OddOneOutTest()
    expected = {
        'attempted': 19.0,
        'errors': 5.0,
        'max': 15.0
    }

    actual = t.parse_raw(raw_score_str)

    assert actual == expected


def test_OOO_parse_session():
    t = OddOneOutTest()
    expected = {
        'num_errors': 5,
        'num_correct': 14,
        'num_attempts': 19,
        'accuracy': 0.736842,
        'better_than_chance': True,
        'duration_ms': 180029,
        'avg_ms_correct': 10167.1428,
        'std_ms_correct': 6473.5386
    }

    actual = t.parse_session(
        session_data_str=test_data['Session Data']['Odd One Out'][0],
        extract_features=True
    )

    for f, v in actual.items():
        assert_almost_equal(v, expected[f], err_msg=f, decimal=2)


def test_question_difficulty_OOO():

    q = {"attributes": {"realDifficulty": 2}}
    expected = 2
    t = OddOneOutTest()

    actual = t.question_difficulty(q)

    assert expected == actual


def test_DoubleTroubleTest():
    assert DoubleTroubleTest._id == 13
    assert DoubleTroubleTest._abbrev == "DT"
    assert DoubleTroubleTest._r_hc == 0.822693
    assert DoubleTroubleTest._hc_feature == "final_score"
    assert DoubleTroubleTest._domain_feature == "final_score"


def test_parse_raw_DoubleTroubleTest():
    raw_score_str = test_data['Legacy Raw Score']["Double Trouble"][0]
    t = DoubleTroubleTest()
    expected = {
        'pct_CC': 1,
        'pct_CI': 0.5556,
        'pct_IC': 1,
        'pct_II': 0.3,
        'RT_CC': 1885.3333,
        'RT_CI': 2816.2222,
        'RT_IC': 2582.375,
        'RT_II': 2602.5
    }

    actual = t.parse_raw(raw_score_str)

    for f, v in actual.items():
        assert_almost_equal(v, expected[f], err_msg=f, decimal=2)


def test_DT_parse_session():
    t = DoubleTroubleTest()
    expected = {
        'num_errors': 11,
        'num_correct': 25,
        'num_attempts': 36,
        'accuracy': 0.6944,
        'better_than_chance': True,
        'duration_ms': 90003,
        'avg_ms_correct': 2496.04,
        'std_ms_correct': 1048.8718
    }

    actual = t.parse_session(
        session_data_str=test_data['Session Data']['Double Trouble'][0],
        extract_features=True
    )
    actual.pop('qs_by_type')

    for f, v in actual.items():
        assert_almost_equal(v, expected[f], err_msg=f, decimal=2)


def case_IR_errors_example_data():
    t = DoubleTroubleTest()

    raw_features = t.parse_raw(
        test_data['Legacy Raw Score']["Double Trouble"][0]
    )
    raw_features['expected_DT_IR_errors'] = -0.35
    return raw_features


def case_IR_errors_simple_neg_slope():
    return {'pct_CC': 1, 'pct_IC': 0.8, 'pct_II': 0.6,
            'expected_DT_IR_errors': -0.2}


def case_IR_errors_simple_positive_slope():
    return {'pct_CC': 0, 'pct_IC': 0.5, 'pct_II': 1,
            'expected_DT_IR_errors': 0.5}


def case_IR_errors_simple_zero_slope():
    return {'pct_CC': 0, 'pct_IC': 0, 'pct_II': 0,
            'expected_DT_IR_errors': 0}


@parametrize_with_cases('DT_feature_data', cases=".", prefix="case_IR_errors")
def test_DT_IR_errors(DT_feature_data):
    t = DoubleTroubleTest()
    calculated_feature_value = t.calc_IR_errors(DT_feature_data)
    assert_almost_equal(calculated_feature_value, DT_feature_data['expected_DT_IR_errors'],
                        err_msg=1, decimal=2)


def case_IR_RT_example_data():
    t = DoubleTroubleTest()

    raw_features = t.parse_raw(
        test_data['Legacy Raw Score']["Double Trouble"][0]
        )
    raw_features['expected_DT_IR_RT'] = 358.58333333333377
    return raw_features


def case_IR_RT_simple_neg_slope():
    return {'RT_CC': 3000, 'RT_IC': 2500, 'RT_II': 2000,
            'expected_DT_IR_RT': -500}


def case_IR_RT_simple_positive_slope():
    return {'RT_CC': 2000, 'RT_IC': 2500, 'RT_II': 3000,
            'expected_DT_IR_RT': 500}


def case_IR_RT_simple_zero_slope():
    return {'RT_CC': 0, 'RT_IC': 0, 'RT_II': 0,
            'expected_DT_IR_RT': 0}


@parametrize_with_cases('DT_feature_data', cases=".", prefix="case_IR_RT")
def test_DT_IR_RT(DT_feature_data):
    t = DoubleTroubleTest()
    calculated_feature_value = t.calc_IR_RT(DT_feature_data)
    assert_almost_equal(calculated_feature_value, DT_feature_data['expected_DT_IR_RT'],
                        err_msg=1, decimal=2)


def test_question_difficulty_DT():

    q = {"problemCode": "II"}
    expected = 2
    t = DoubleTroubleTest()
    actual = t.question_difficulty(q)
    assert actual == expected


# Polygon Test


def test_PolygonsTest():
    assert PolygonsTest._id == 23
    assert PolygonsTest._abbrev == "PO"
    assert PolygonsTest._r_hc == 0.523616
    assert PolygonsTest._hc_feature == "final_score"
    assert PolygonsTest._domain_feature == "final_score"


def test_parse_raw_PolygonsTest():
    raw_score_str = test_data['Legacy Raw Score']["Polygons"][0]
    t = PolygonsTest()
    expected = {}

    actual = t.parse_raw(raw_score_str)

    assert actual == expected


def test_PO_parse_session():
    t = PolygonsTest()
    expected = {
        'num_errors': 6,
        'num_correct': 15,
        'num_attempts': 21,
        'accuracy': 0.7143,
        'better_than_chance': True,
        'duration_ms': 90037,
        'avg_ms_correct': 3508.5333,
        'std_ms_correct': 1883.7755
    }

    actual = t.parse_session(
        session_data_str=test_data['Session Data']['Polygons'][0],
        extract_features=True
    )

    for f, v in actual.items():
        assert_almost_equal(v, expected[f], err_msg=f, decimal=2)


def test_question_difficulty_PO():

    q = {"difficulty": 1}
    expected = 1
    t = PolygonsTest()

    actual = t.question_difficulty(q)

    assert expected == actual


# Spatial Planning Test


def test_SpatialPlanningTest():
    assert SpatialPlanningTest._id == 15
    assert SpatialPlanningTest._abbrev == "SP"
    assert SpatialPlanningTest._r_hc == 0.731601
    assert SpatialPlanningTest._hc_feature == "final_score"
    assert SpatialPlanningTest._domain_feature == "final_score"


def test_SP_parse_session():
    t = SpatialPlanningTest()
    expected = {
        'num_errors': 3,
        'num_correct': 3,
        'num_attempts': 6,
        'accuracy': 0.5,
        'better_than_chance': True,
        'duration_ms': 180040,
        'avg_ms_correct': 17806.6667,
        'std_ms_correct': 5758.3857
    }

    actual = t.parse_session(
        session_data_str=test_data['Session Data']['Spatial Planning'][0],
        extract_features=True
    )

    for f, v in actual.items():
        assert_almost_equal(v, expected[f], err_msg=f, decimal=2)


def test_question_difficulty_SP():

    q = {"attributes": {"foundTokenNum": 0,
                        "tokenIndex": 9,
                        "difficultyLevel": 5}}
    expected = 5
    t = SpatialPlanningTest()

    actual = t.question_difficulty(q)

    assert expected == actual


# Grammatical Reasoning Test


def test_GrammaticalReasoningTest():
    assert GrammaticalReasoningTest._id == 16
    assert GrammaticalReasoningTest._abbrev == "GR"
    assert GrammaticalReasoningTest._r_hc == 0.758347
    assert GrammaticalReasoningTest._hc_feature == "final_score"
    assert GrammaticalReasoningTest._domain_feature == "final_score"


def test_GR_parse_session():
    t = GrammaticalReasoningTest()
    expected = {
        'num_errors': 2,
        'num_correct': 11,
        'num_attempts': 13,
        'accuracy': 0.8462,
        'better_than_chance': True,
        'duration_ms': 90034,
        'avg_ms_correct': 6138.3636,
        'std_ms_correct': 856.40997
    }

    actual = t.parse_session(
        session_data_str=test_data['Session Data']['Grammatical Reasoning'][0],
        extract_features=True
    )

    for f, v in actual.items():
        assert_almost_equal(v, expected[f], err_msg=f, decimal=2)


def test_question_difficulty_GR():

    parsed_question = {
        "questionText": "square does not contain circle",
        "outer": "square",
    }
    expected = 2
    t = GrammaticalReasoningTest()

    actual = t.question_difficulty(parsed_question)

    assert expected == actual


@pytest.mark.parametrize(
    "exclude, abbrev, expected",
    [
        ([], False, EXPECTED_FEATURES_V7),
        (FEATURES_TO_EXCLUDE_V3, False, EXPECTED_FEATURES_V8),
        ([], True, EXPECTED_ABBREVIATED_FEATURES_V1),
        (FEATURES_TO_EXCLUDE_V3, True, EXPECTED_ABBREVIATED_FEATURES_V2)
    ],
)
def test_timing_features(exclude, abbrev, expected):
    actual = timing_features(exclude=exclude, abbrev=abbrev)

    assert actual == expected


def test_abbrev_features():
    feature_list = [
        ("digit_span", "avg_score"),
        ("spatial_span", "avg_score"),
        ("feature_match", "final_score"),
    ]
    expected = ["DS_avg_score", "SS_avg_score", "FM_final_score"]

    actual = abbrev_features(feature_list)

    assert expected == actual


def test_abbrev_columns():
    expected = ["DS_avg_score", "SS_avg_score", "FM_final_score"]
    mockDf = MagicMock()
    mockDf.columns = [
        ("digit_span", "avg_score"),
        ("spatial_span", "avg_score"),
        ("feature_match", "final_score"),
    ]

    actual = abbrev_columns(mockDf)

    assert expected == actual.columns


@pytest.mark.parametrize(
    "app, type_, exception",
    [
        ("ddd", "feature", ValueError("Invalid application ddd")),
        ("hc", "fff", ValueError("Invalid argument fff")),
    ],
)
def test_test_features_exceptions(app, type_, exception):
    try:
        test_features(app=app, type_=type_)
    except ValueError as inst:
        # First ensure the exception is of the right type
        assert isinstance(inst, type(exception))
        # Ensure that exceptions have the same message
        assert inst.args == exception.args
    else:
        pytest.fail("Expected error but found none")


def test_test_features_no_exception():
    expected = EXPECTED_FEATURES_V11

    actual = test_features(app="domain", type_="feature")

    assert expected == actual


def test_domain_feature_list():
    expected = EXPECTED_FEATURES_V11

    actual = domain_feature_list()

    assert expected == actual


def test_hc_feature_list():
    expected = EXPECTED_HC_FEATURES

    actual = hc_feature_list()

    assert expected == actual


def test_tests_of_type_incorrect_type():
    try:
        tests_of_type("")
    except AttributeError as inst:
        # First ensure the exception is of the right type
        assert isinstance(inst, AttributeError)
        # Ensure that exceptions have the same message
        assert inst.args[0] == "Invalid test type "
    else:
        pytest.fail("Expected error but found none")


def test_memory_tests():
    expected = [
        "spatial_span",
        "monkey_ladder",
        "digit_span",
        "paired_associates",
        "token_search",
    ]

    actual = memory_tests()

    assert actual == expected


def test_reasoning_tests():
    expected = ["double_trouble", "odd_one_out", "rotations", "feature_match"]

    actual = reasoning_tests()

    assert actual == expected


# dataframes


def test_set_column_names_unequal_names_length():
    try:
        mockDf = MagicMock()
        mockDf.columns = ["a"]
        names = ["c", "d"]
        set_column_names(mockDf, names)
    except AssertionError as inst:
        # First ensure the exception is of the right type
        assert isinstance(inst, AssertionError)
    else:
        pytest.fail("Expected error but found none")


def test_set_column_names():
    mockDf = MagicMock()
    mockDf.columns = ["a", "b"]
    names = ["c", "d"]
    expected = ["c", "d"]

    set_column_names(mockDf, names)

    assert mockDf.columns == expected


def test_abbreviate_columns():
    mockDf = MagicMock()
    mockDf.columns = [
        ("digit_span", "avg_score"),
        ("spatial_span", "avg_score"),
        ("feature_match", "final_score"),
    ]
    expected = ["DS_avg_score", "SS_avg_score", "FM_final_score"]

    actual = abbreviate_columns(mockDf)

    assert actual.columns == expected


@pytest.fixture
def test_names_fixture():
    return [
        "spatial_span",
        "grammatical_reasoning",
        "double_trouble",
        "odd_one_out",
        "monkey_ladder",
        "rotations",
        "feature_match",
        "digit_span",
        "spatial_planning",
        "paired_associates",
        "polygons",
        "token_search",
    ]


@pytest.fixture
def rotated_pca_loadings():
    return np.array(
        [
            [0.7014310832595965, 0.2114602614099599, 0.06335835494755199],
            [0.06489246556607997, 0.3200071012586953, 0.6555755842264829],
            [0.21528601455159402, 0.3526924332953027, 0.5250830836410911],
            [0.1992332685797254, 0.5526963820789189, -0.11183812310895136],
            [0.6968350824881397, 0.2277093563746992, 0.053842026805891285],
            [0.12127517611619794, 0.6653527253557938, 0.09299029746874055],
            [0.16663501355802068, 0.5504977638792241, 0.2190648077992447],
            [0.2697350970669865, -0.20477442132929694, 0.7086057589964939],
            [0.4015791806929251, 0.45692510567203554, 0.03807692316482178],
            [0.5987583784444802, -0.03013309539255579, 0.23366793983112322],
            [-0.00047574212977022357, 0.5198168539290584, 0.3378749934189247],
            [0.6001410364160199, 0.1816371349537731, 0.18355395542544883],
        ]
    )


@pytest.fixture
def domain_names():
    return ["STM", "reasoning", "verbal"]


@pytest.fixture
def covariance_fixture():
    return np.array(
        [
            [
                1.00001877,
                0.17657497,
                0.25456675,
                0.15975593,
                0.41123286,
                0.22052783,
                0.23950432,
                0.16941961,
                0.25657813,
                0.24761691,
                0.1748068,
                0.33267418,
            ],
            [
                0.17657497,
                1.00001877,
                0.31134129,
                0.13863341,
                0.20351513,
                0.20829056,
                0.22292791,
                0.21532425,
                0.19143197,
                0.16329805,
                0.18374061,
                0.19766977,
            ],
            [
                0.25456675,
                0.31134129,
                1.00001877,
                0.12745101,
                0.24312973,
                0.21874051,
                0.24699616,
                0.18504348,
                0.24972915,
                0.18741918,
                0.20326165,
                0.25492511,
            ],
            [
                0.15975593,
                0.13863341,
                0.12745101,
                1.00001877,
                0.16839422,
                0.20850424,
                0.18164247,
                0.05334255,
                0.17095683,
                0.10991068,
                0.13512469,
                0.12871365,
            ],
            [
                0.41123286,
                0.20351513,
                0.24312973,
                0.16839422,
                1.00001877,
                0.20693492,
                0.23425227,
                0.17376448,
                0.27081193,
                0.25982165,
                0.15926039,
                0.31415826,
            ],
            [
                0.22052783,
                0.20829056,
                0.21874051,
                0.20850424,
                0.20693492,
                1.00001877,
                0.26734509,
                0.06536186,
                0.2462393,
                0.13388592,
                0.22042753,
                0.17894845,
            ],
            [
                0.23950432,
                0.22292791,
                0.24699616,
                0.18164247,
                0.23425227,
                0.26734509,
                1.00001877,
                0.1109622,
                0.21611095,
                0.16251798,
                0.23562067,
                0.20001099,
            ],
            [
                0.16941961,
                0.21532425,
                0.18504348,
                0.05334255,
                0.17376448,
                0.06536186,
                0.1109622,
                1.00001877,
                0.08189992,
                0.20469298,
                0.10205455,
                0.17487756,
            ],
            [
                0.25657813,
                0.19143197,
                0.24972915,
                0.17095683,
                0.27081193,
                0.2462393,
                0.21611095,
                0.08189992,
                1.00001877,
                0.18194598,
                0.17319142,
                0.25291604,
            ],
            [
                0.24761691,
                0.16329805,
                0.18741918,
                0.10991068,
                0.25982165,
                0.13388592,
                0.16251798,
                0.20469298,
                0.18194598,
                1.00001877,
                0.11709855,
                0.24620818,
            ],
            [
                0.1748068,
                0.18374061,
                0.20326165,
                0.13512469,
                0.15926039,
                0.22042753,
                0.23562067,
                0.10205455,
                0.17319142,
                0.11709855,
                1.00001877,
                0.17115672,
            ],
            [
                0.33267418,
                0.19766977,
                0.25492511,
                0.12871365,
                0.31415826,
                0.17894845,
                0.20001099,
                0.17487756,
                0.25291604,
                0.24620818,
                0.17115672,
                1.00001877,
            ],
        ]
    )


def test_component_loadings(rotated_pca_loadings,
                            test_names_fixture,
                            domain_names):

    expected = pd.DataFrame(
        data=rotated_pca_loadings,
        index=test_names_fixture,
        columns=domain_names
    )

    actual = component_loadings()

    assert expected.equals(actual)


def test_covariance(covariance_fixture, test_names_fixture):
    expected = pd.DataFrame(
        data=covariance_fixture,
        index=test_names_fixture,
        columns=test_names_fixture
    )

    actual = covariance()

    assert expected.equals(actual)
