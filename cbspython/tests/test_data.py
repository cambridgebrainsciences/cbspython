FEATURES_TO_EXCLUDE_V3 = [
    "double_trouble",
    "monkey_ladder",
    "feature_match",
    "digit_span",
    "spatial_planning",
    "paired_associates",
]

EXPECTED_FEATURES_V7 = [
    ("spatial_span", "avg_ms_per_item"),
    ("grammatical_reasoning", "avg_ms_correct"),
    ("double_trouble", "avg_ms_correct"),
    ("odd_one_out", "avg_ms_correct"),
    ("monkey_ladder", "avg_ms_per_item"),
    ("rotations", "avg_ms_correct"),
    ("feature_match", "avg_ms_correct"),
    ("digit_span", "avg_ms_per_item"),
    ("spatial_planning", "avg_ms_correct"),
    ("paired_associates", "avg_ms_per_item"),
    ("polygons", "avg_ms_correct"),
    ("token_search", "avg_ms_per_item"),
]

EXPECTED_FEATURES_V8 = [
    ("spatial_span", "avg_ms_per_item"),
    ("grammatical_reasoning", "avg_ms_correct"),
    ("odd_one_out", "avg_ms_correct"),
    ("rotations", "avg_ms_correct"),
    ("polygons", "avg_ms_correct"),
    ("token_search", "avg_ms_per_item"),
]

EXPECTED_ABBREVIATED_FEATURES_V1 = [
    "SS_avg_ms_per_item",
    "GR_avg_ms_correct",
    "DT_avg_ms_correct",
    "OOO_avg_ms_correct",
    "ML_avg_ms_per_item",
    "RT_avg_ms_correct",
    "FM_avg_ms_correct",
    "DS_avg_ms_per_item",
    "SP_avg_ms_correct",
    "PA_avg_ms_per_item",
    "PO_avg_ms_correct",
    "TS_avg_ms_per_item",
]

EXPECTED_ABBREVIATED_FEATURES_V2 = [
    "SS_avg_ms_per_item",
    "GR_avg_ms_correct",
    "OOO_avg_ms_correct",
    "RT_avg_ms_correct",
    "PO_avg_ms_correct",
    "TS_avg_ms_per_item",
]

# for tests `test_test_features_no_exception`
# and `test_domain_feature_list`:
EXPECTED_FEATURES_V11 = [
    ("spatial_span", "max_score"),
    ("grammatical_reasoning", "final_score"),
    ("double_trouble", "final_score"),
    ("odd_one_out", "max"),
    ("monkey_ladder", "max_score"),
    ("rotations", "final_score"),
    ("feature_match", "final_score"),
    ("digit_span", "max_score"),
    ("spatial_planning", "final_score"),
    ("paired_associates", "max_score"),
    ("polygons", "final_score"),
    ("token_search", "max_score"),
]

# for test `test_hc_feature_list`:
EXPECTED_HC_FEATURES = [
    ("spatial_span", "avg_score"),
    ("grammatical_reasoning", "final_score"),
    ("double_trouble", "final_score"),
    ("odd_one_out", "final_score"),
    ("monkey_ladder", "avg_score"),
    ("rotations", "final_score"),
    ("feature_match", "final_score"),
    ("digit_span", "avg_score"),
    ("spatial_planning", "final_score"),
    ("paired_associates", "avg_score"),
    ("polygons", "final_score"),
    ("token_search", "avg_score"),
]
