# Testing the Norms class in cbsnorm.py

import pytest
import os.path as path
import hashlib
import numpy as np
import cbspython as cbs

from cbspython.cbsnorms import Norms, ApplicationNorms
from unittest.mock import MagicMock

# 1. Test if the class can be imported from this package
# (i.e., is available externally)


def test_norms_class_import():
    try:
        from cbspython import Norms  # noqa: F401
    except ImportError:
        pytest.fail("Error: can not import the Norms class from cbspython")

# 2. Test if all the data files are there with the correct names


def test_if_file_exists_for_norms():
    filePath = path.join(
        path.dirname(cbs.__file__), 'data', 'summary_norms_1yr_bins.csv')
    assert path.exists(filePath)

# 3. Test if the data file has changed:


def test_check_norms_hash():
    expected_norms_hash = "9d954a5e01294a33a8adc46e83b0632b"

    filename = path.join(
        path.dirname(cbs.__file__), 'data', 'summary_norms_1yr_bins.csv')
    md5_hash = hashlib.md5()
    # get the hash of the current file
    with open(filename, "rb") as f:
        # Read and update hash in chunks of 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            md5_hash.update(byte_block)
    actual_norms_hash = md5_hash.hexdigest()

    assert actual_norms_hash == expected_norms_hash

# 4. Tests for Norms getter


def test_data_directory(mocker):
    expected = "."
    n = Norms()
    mocker.patch('cbspython.cbsnorms.path.exists', return_value=False)

    actual = n.data_directory

    assert actual == expected

# 5. Tests for Norms methods


def test_data_slice():
    # TODO
    pass


def test_num_data_points(mocker):
    SCORE_IDX = ['test', 'feature']
    expected = 1

    # Create mock objects
    mocked_count_0 = MagicMock(name="mocked_count_2")
    mocked_count_0.sum = MagicMock(return_value=1)

    mocked_count_1 = MagicMock(name="mocked_count_1")
    mocked_count_1.unstack = MagicMock(return_value=mocked_count_0)

    mocked_data_frame = MagicMock(name="mocked_data_frame")
    mocked_data_frame.xs = MagicMock(return_value=mocked_count_1)

    actual = Norms().num_data_points(mocked_data_frame)

    # check method calls
    mocked_data_frame.xs.assert_called_once_with('count', level=1, axis=1)
    mocked_count_1.unstack.assert_called_once_with(SCORE_IDX)
    mocked_count_0.sum.assert_called_once_with(axis=0)
    assert actual == expected


def test_query_for_norms():
    # TODO
    pass

# Testing the ApplicationNorms class in cbsnorm.py

# 1. Test if the class can be imported from this package
# (i.e., is available externally)


def test_ApplicationNorms_class_import():
    try:
        from cbspython import ApplicationNorms  # noqa: F401
    except ImportError:
        pytest.fail(
            "Error: can not import the ApplicationNorms class from cbspython")

# 2. Test if all the data files are there with the correct names


def test_if_file_exists_for_ApplicationNorms():
    assert path.exists(
        path.join(path.dirname(cbs.__file__), 'data', 'norms_master.pickle'))

# 3. Test if the data file has changed:


def test_check_appNorms_hash():
    expected_appNorms_hash = "4459d4ef2c1991cbf28a00ef73b41fec"

    filename = path.join(
        path.dirname(cbs.__file__), 'data', 'norms_master.pickle')
    md5_hash = hashlib.md5()
    # get the hash of the current file
    with open(filename, "rb") as f:
        # Read and update hash in chunks of 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            md5_hash.update(byte_block)
    actual_appNorms_hash = md5_hash.hexdigest()

    assert actual_appNorms_hash == expected_appNorms_hash

# 4. Test for ApplicationNorms getter


def test_age_bin_edges():
    expected = np.array(
        [-np.Inf, 4, 6, 8, 10, 13, 16, 18, 25, 35, 45, 55, 65, 75, 98, np.Inf])

    actual = ApplicationNorms().age_bin_edges

    assert (actual == expected).all()

# 5. Tests for Norms methods


def test_hc_age_bins():
    expected = [4.0, 6.0, 8.0, 10.0, 13.0, 16.0,
                18.0, 25.0, 35.0, 45.0, 55.0, 65.0]

    actual = ApplicationNorms().hc_age_bins(66)

    assert actual == expected


def test_age_bins_for():
    expected = [slice(65.0, 74.0, None)]

    actual = ApplicationNorms().age_bins_for(66.0)

    assert actual == expected

# Testing the ApplicationNorms2 class in cbsnorm.py

# 1. Test if the class can be imported from this package
# (i.e., is available externally)


def test_ApplicationNorms2_class_import():
    try:
        from cbspython import ApplicationNorms2  # noqa: F401
    except ImportError:
        pytest.fail(
            "Error: can not import the ApplicationNorms2 class from cbspython")
