import numpy as np
import re
from scipy import linalg
from .cbstests import ROTATED_PCA_LOADINGS, COVARIANCE, TESTS
from os import path

import collections
import six


def iterable(arg):
    """ Checks if an item is iterable, and not a string.

    Args:
        arg (*): The variable that we wish to check if is iterable.

    Returns:
        Boolean: Is it iterable and not a string?
    """
    return (
        isinstance(arg, collections.Iterable)
        and not isinstance(arg, six.string_types)
    )


class ArgumentValueError(Exception):
    """ Exception raised when the provided argument is an invalid option.
    """
    def __init__(self, opt_name, opt_val, valid_vals):
        self.message = (
            f"but valid values are [{', '.join([str(o) for o in valid_vals])}]"
        )
        self.opt_name = opt_name
        self.opt_val = opt_val
        self.valid_vals = valid_vals
        super().__init__(self.message)

    def __str__(self):
        return f"{self.opt_name} -> {self.opt_val}, {self.message}"


def check_arg_value(arg_name, arg_vals, valid_vals):
    """ Simple verification for arguments that require a valid option.

    Args:
        arg_name (str): The name of the argument.
        arg_val (str, numeric): The supplied value of the argument
        valid_vals (list): A list of argument values that are allowed.

    Raises:
        ValueError: The supplied argument value is invalid.

    Examples:
        >>> check_arg_value('arg1', 1, [1, 2, 3, 4, 5])

        >>> check_arg_value('arg2', 6, [1, 2, 3, 4, 5])
        ArgumentValueError: arg2 -> [6], but valid values are [1, 2, 3, 4, 5]           # noqa: E501

        >>> check_arg_value('arg3', [1, 2], [1, 2, 3, 4, 5])

        >>> check_arg_value('arg4', [1, 6], [1, 2, 3, 4, 5])
        >>> ArgumentValueError: arg4 -> [1, 6], but valid values are [1, 2, 3, 4, 5]    # noqa: E501

        >>> check_arg_value('arg5', 'cat', ['cat', 'dog', 'mouse'])

        >>> check_arg_value('arg6', ['a', 'c'], ['cat', 'dog', 'mouse'])
        ArgumentValueError: arg6 -> ['a', 'c'], but valid values are ['cat', 'dog', 'mouse']  # noqa: E501

    """
    if not iterable(arg_vals):
        arg_vals = [arg_vals]

    if not all([val in valid_vals for val in arg_vals]):
        raise ArgumentValueError(arg_name, arg_vals, valid_vals)


def default_data_directory():
    """ Returns the default location for data files included with this package.

    Returns:
        str: The full path to the data directory.
    """
    full_path = path.join(path.dirname(__file__), 'data')
    if path.exists(full_path):
        return full_path
    else:
        return '.'


def force_column_vector(x):
    """Forces an input to be a column vector

    Arguments:
        x {array-like} -- the data to be forced into a column vector shape
    """
    return np.reshape(x, (x.size, -1))


def weighted_mean(x, weights=None, axis=0):
    """ Calculates the weighted mean(s) across some dimension of the matrix X.
        Note, not yet tested for X with dimensions > 2.

    Arguments:
        x {array-like} -- N-D matrix of data to be averaged.

    Keyword Arguments:
        weights {array-like} --  N-D matrix of data to be averaged. A M-D
        matrix of weights for each sample data point to be averaged. Should
        have the same length as x along the axis to be averaged. If M>1
        (weights is a > 1D matrix), then the other dimension can be other sets
        of weights. For example, if averaging over rows of X, then row values
        of 'weights' are the weight values, and columns indicatea different set
        of weights. This is useful for calcuating multiple weighted averages in
        one shot.

        axis {int} -- Average over rows(0) or columns(1) (default: {0})

    Returns:
        [type] -- [description]
    """

    x = np.asanyarray(x)
    if weights is None:
        w_size = np.array([1, 1])
        w_size[axis] = x.shape[axis]
        weights = np.ones(w_size)
    else:
        weights = np.asanyarray(weights)
        if x.shape[axis] != weights.shape[axis]:
            raise ValueError(
                f"Length of weights ({weights.shape[axis]}) not "
                f"equal to length of x ({x.shape[axis]})."
            )

    x = np.atleast_2d(x)
    w = np.atleast_2d(weights)

    if axis == 0:
        c = np.ones([1, x.shape[axis]])
        sums_of_w = np.dot(c, w)
        wsum_of_x = np.dot(w.T, x)

    elif axis == 1:
        c = np.ones([x.shape[axis], 1])
        sums_of_w = np.dot(w, c)
        wsum_of_x = np.dot(x, w.T)

    return np.squeeze(np.diag(wsum_of_x) / np.squeeze(sums_of_w))


def pooled_vars_and_means(vars_, counts, means=None):
    """ Calculates the pooled variances (or covariance matrix) and means of
        multiple measures from multiple (differently sized) samples. If you are
        calculating pooled covariance, then the sample sizes must be the same
        within a group (i.e., across features within a group).

    Arguments:

        vars_ {array-like} -- M X G array-like of sample variances.
            M = # of measures, G = # of sample groups. Alternatively, vars_
            can be a M x M x G set of covariance matrices.
        counts {array-like} -- M X G array-like of sample counts.
            M = # of measures, G = # of sample groups. Sample sizes must be the
            same for each measure within a group, which means that all values
            within a column are the same (though can be different between
            columns - i.e., groups.)

    Keyword Arguments:
        means {array-like} -- M X G array-like of sample means. If not
            provided, assume a mean of zero for each measure and group.
            M = # of measures, G = # of sample groups. (default: {None})

    Returns:
        list -- the final sample size(1st element), the pooled mean (2nd), and
        standard deviation (3rd).
    """
    if means is None:
        means = np.zeros_like(counts)
    else:
        if means.shape != counts.shape:
            raise ValueError(
                f"Size of means({means.shape}) not equal to size"
                f" of counts ({counts.shape})."
            )

    n_grps = counts.shape[1]
    n_meas = counts.shape[0]

    if len(vars_.shape) < 3:
        do_covars = False
        if vars_.shape != counts.shape:
            raise ValueError(f"Size of variances ({vars_.shape}) not equal to "
                             f" size of counts ({counts.shape}).")
    else:
        do_covars = True
        if vars_.shape[0] != vars_.shape[1]:
            raise ValueError(f"Variance shape indicates covariance matrices, "
                             f"but not square in 1st/2nd dims {vars_.shape}")
        if vars_.shape[0] != n_meas:
            raise ValueError(f"Number of measures in covariances matrices "
                             f"{vars_.shape[0]} does not equal other inputs "
                             f"(counts = {counts.dim}")
        if vars_.shape[2] != n_grps:
            raise ValueError(f"Variance shape indicates covariance matrices, "
                             f"but 3rd dim {vars_.shape} does not equal the "
                             f"of groups {n_grps}")

    w_means = weighted_mean(means, counts, axis=1)
    w_means = force_column_vector(w_means)
    s_final = np.zeros([n_meas, n_meas])
    for g in range(n_grps):
        if do_covars:
            n = counts[0, g]
            if np.any(counts[:, g] != n):
                raise ValueError(f"Counts for measures in group {g} are not "
                                 f"all equal, see -> ({counts[:,g]}) ")
        else:
            n = np.diagflat(counts[:, g])
        s = vars_[:, :, g] if do_covars else np.diagflat(vars_[:, g])
        d = (means[:, [g]]-w_means)  # Group differences from new weighted mean
        s_final += (n-1) * s + n * np.dot(d, d.T)

    if do_covars:
        n_total = counts[0, :].sum()
    else:
        n_total = np.diagflat(counts.sum(axis=1))

    s_final = s_final / (n_total-1)
    s_final = np.diag(s_final) if not do_covars else s_final
    n_total = np.diag(n_total) if not do_covars else n_total
    return (
        np.squeeze(
            np.repeat(n_total, n_meas) if do_covars else np.squeeze(n_total)
        ),
        np.squeeze(w_means),
        np.squeeze(s_final)
    )


def domain_input_mask(recipe):
    """ Converts a recipe id (an int of binary flags to indicate the absence
        of test scores in a domain score calculation) into a boolean array
        that indicates whether a test is included in the calculation.
    """
    present = [not bool(int(x)) for x in np.binary_repr(recipe, width=12)]
    return np.array(present)


def domain_recipes_from_inputs(inputs):
    """ Calculates a recipe identifier for a collection of scores that will
        be used to calculate domain scores. The identifier is a binary mask
        (integer) that indicates the presence (non-nan value) of a score.

    Arguments:
        inputs {array-like} -- N x 12 matrix of test scores, N = # subjects
    """
    nullvals = inputs.isnull().to_numpy()
    coeffs = 2 ** (np.arange(12, 0, -1) - 1)
    return np.dot(nullvals, coeffs)


def domain_weights(recipe=None, s_overall=COVARIANCE):
    """ Calculates the weighting factors for each test to contribute to each
        domain score. TODO: update this to include the Nelson references and
        describe how re-weigting works.

    Arguments:
        recipe {int, or boolean array} --
    """
    c = linalg.pinv(ROTATED_PCA_LOADINGS).T

    if recipe is not None:
        recipe = np.asanyarray(recipe)
        if recipe.size == 1:
            recipe = domain_input_mask(recipe)
        if recipe.size != c.shape[0]:
            raise ValueError("Input mask (recipe) has incompatible dims.")

        obs_i = recipe
        mis_i = np.logical_not(obs_i)
        s_22 = s_overall[np.ix_(obs_i, obs_i)]
        s_21 = s_overall[np.ix_(obs_i, mis_i)]
        s_22i = linalg.inv(s_22)
        obs_c = c[obs_i, :]
        mis_c = c[mis_i, :]
        c = np.dot(s_22i, np.dot(s_21, mis_c)) + obs_c

    return c


def linear_combination_of_RVs(means, covar, weights):
    """[summary]
    Arguments:
        means {array-like} -- N x M matrix of variable means. N = # of sets,
            M = # of variables.
        covar {array-like} -- M x M x N covariance matrices.
        weights {array-like} -- M x P matrix of weights, to produce P new
            variables from M input variables.
    """
    # TODO add argument size checking for compatible dimensions
    n_outputs = weights.shape[1]
    n_sets = covar.shape[2]

    new_means = np.dot(means, weights)
    new_covar = np.empty([n_outputs, n_outputs, n_sets]) * np.nan
    for i in range(n_sets):
        new_covar[:, :, i] = np.dot(weights.T, np.dot(covar[:, :, i], weights))

    return (new_means, new_covar)


first_cap_re = re.compile('(.)([A-Z][a-z]+)')
all_cap_re = re.compile('([a-z0-9])([A-Z])')
space_re = re.compile(r'\s+')


def to_camelcase(string):
    """ Convert a space separated and capitalized (i.e. titleized) string into
        an underscore and lower-case form (i.e., snakecase)

    Arguments:
        string {string} -- a string it title format

    Returns:
        string -- the string in snakecase form
    """
    s0 = re.sub(space_re, '', string)
    s1 = first_cap_re.sub(r'\1_\2', s0)
    return all_cap_re.sub(r'\1_\2', s1).lower()


def varimax(factor_loadings):
    num_components = factor_loadings.shape[1]
    num_variables = factor_loadings.shape[0]
    d = 0
    R = np.eye(num_components)
    for i in range(1, 100):
        d_old = d
        z = np.dot(factor_loadings, R)
        B = np.dot(factor_loadings.T, z**3 - (1/num_variables)
                   * np.dot(z, np.diag(np.diag(np.dot(z.T, z)))))
        u, s, vh = linalg.svd(B)
        R = np.dot(u, vh)
        d = sum(s)
        if d_old != 0 and d < (d_old * (1 + 1e-7)):
            break
    return R

# FUNCTIONS FOR DEALING WITH LEGACY DATA


def legacy_name_mapper(test, score):
    """ Basically, maps from new to old score names, for each test. We need
        this because the clean dataset uses outdated naming conventions. """
    append = None
    if test in [
        'spatial_span', 'paired_associates', 'digit_span',
        'token_search', 'monkey_ladder'
    ]:
        append = {'max_score': 'max_score',
                  'avg_score': 'avg_score'}.get(score, None)
    elif test in ['feature_match', 'odd_one_out', 'rotations', 'polygons']:
        append = {'final_score': 'score', 'attempted': 'score_Attempted',
                  'errors': 'score_Errors', 'max': 'score_Max',
                  'correct_score': 'score_CorrectScore'}.get(score, None)
    elif test in ['double_trouble']:
        append = {'final_score': 'score', 'pct_CC': 'score_field_1',
                  'pct_CI': 'score_field_2', 'pct_IC': 'score_field_3',
                  'pct_II': 'score_field_4', 'RT_CC': 'score_field_5',
                  'RT_CI': 'score_field_6', 'RT_IC': 'score_field_7',
                  'RT_II': 'score_field_8'}.get(score, None)
    elif test in ['grammatical_reasoning', 'polygons', 'spatial_planning']:
        append = {'final_score': 'score', }.get(score, None)

    if append is None:
        raise ValueError("Invalid score %s for test %s" % (score, test))
    return test+"_"+append


def rename_legacy_columns():
    """ Some legacy columns might need to be renamed. Indicate in here. """
    return {'spatial_tree_score': 'spatial_planning_score'}


def all_legacy_score_names():
    """ Returns a list of all score names/features in legacy form """
    return [legacy_name_mapper(test_name, score) for (test_name, test)
            in TESTS.items() for score in test.features()]
