###############################################################################
# This file contains classes and functions for performing score calculations.
# It's a bit of a random collection at this point, and it needs to better
# organized, abstracted, etc.=
###############################################################################

import pandas as pd
import numpy as np
from scipy.stats import norm


def _good_data_types():
    """Valid data types when testing inputs"""
    return (pd.DataFrame, pd.Series, np.ndarray, np.number)


class DataTypeError(Exception):
    """Exception raised when a data type is unexpected."""

    def __init__(self, name, data, message=f"not one of {_good_data_types()}"):
        self.message = message
        self.name = name
        self.data = data
        super().__init__(self.message)

    def __str__(self):
        return f"{self.name} -> {self.data.__class__}, {self.message}"


class DataSizeError(Exception):
    """Exception raised when a data variable has an unexpected size."""

    def __init__(self, name, data, expected_shape):
        self.message = f"expected size to be {expected_shape}"
        self.name = name
        self.data = data
        self.shape = expected_shape
        super().__init__(self.message)

    def __str__(self):
        return f"{self.name} -> {self.data.shape}, {self.message}"


def _is_broadcastable(x_shape, y_shape):
    """Checks if two N-D arrays are broadcastable. Inputs are array sizes,
    NOT the arrays themselves.
    """
    return all(
        (m == n) or (m == 1) or (n == 1) for m, n in zip(x_shape[::-1], y_shape[::-1])
    )


def _validate_inputs(**args):
    """Given a set of data inputs, makes sure that they have consistent type
    and shape. Expected shape is determined from the 1st item in the list.
    """
    expected_shape = np.nan
    for i, (nme, arg) in enumerate(args.items()):
        if np.issubdtype(type(arg), np.number):
            arg = np.float_(arg)
            args[nme] = arg
        if not isinstance(arg, _good_data_types()):
            raise DataTypeError(nme, arg)

        if i == 0:
            expected_shape = arg.shape
        else:
            if (arg.shape != expected_shape) & (
                not _is_broadcastable(expected_shape, arg.shape)
            ):
                raise DataSizeError(nme, arg, expected_shape)

    arg_vals = list(args.values())

    return arg_vals if len(arg_vals) > 1 else arg_vals[0]


def _maintain_type(x_orig, x_calc):
    """Convert a set of calculated values back into the original format."""
    if not isinstance(x_calc, np.ndarray):
        x_calc = np.array(x_calc)

    if isinstance(x_orig, pd.DataFrame):
        x_calc = pd.DataFrame(x_calc, index=x_orig.index, columns=x_orig.columns)
    elif isinstance(x_orig, pd.Series):
        x_calc = pd.Series(x_calc, index=x_orig.index)

    return x_calc


def to_z(data, pop_mean, pop_std):
    """Calculates a z-score from a raw score and population parameters.
    This can operate on individual values, arrays, or dataframes I should
    add some type checking in here.
    """
    data, pop_mean, pop_std = _validate_inputs(**locals())
    return (data - pop_mean) / pop_std


def to_adjusted(data, pop_mean, reliability):
    """Performs an adjustment for regression to mean, given some data the
    population mean and the test-retest reliability.
    """
    data, pop_mean, reliability = _validate_inputs(**locals())
    return pop_mean + (data - pop_mean) * reliability


def z_to_p(zscores):
    """Calculates the percentiles (CDFs) from a set of z-scores
    assuming a normal distribution.
    """
    zscores = _validate_inputs(**locals())
    cdfs = norm.cdf(zscores)
    return _maintain_type(zscores, cdfs)


def z_to_score(zscores, pop_mean, pop_std):
    """Given a set of zscores and the population mean/std, convert the
    z-scores back into rack scores.
    """
    zscores, pop_mean, pop_std = _validate_inputs(**locals())
    scores = (zscores * pop_std) + pop_mean
    return _maintain_type(zscores, scores)


def p_to_z(ps):
    """Converts p-values (percentiles, from a CDF) to z-scores. Assumes a
    normal distribution with mean=0 std=1 (inverse of z_to_p)
    """
    ps = _validate_inputs(**locals())
    zscores = norm.ppf(ps)
    return _maintain_type(ps, zscores)


def p_to_rank(p, size=20):
    """Calculates the "rank" in a class of a given size, given the percentile
    (CDF) scores. A rank of 1 indicates the best performer, whereas the
    higherst rank indicates the lowest performer. Basically, this function
    just bins percentiles into N-1 bins, where N is the number of people in
    the class.
    """
    p = _validate_inputs(pvalues=p)
    ranks = ((1 - p) * (size - 1) + 1).round()
    return _maintain_type(p, ranks)


def standard_parameters():
    """Defines the common population mean and average for standardized scores.
    When converting test scores to standardized scores, they will have a
    population mean and standard deviation according to these values.
    """
    return {
        "mean": 100,
        "std": 15,
    }


def to_standard_score(scores, pop_mean, pop_std):
    """Converts the specified score feature to a standardized score.
    Standardized scores all share a common mean and standard deviation.
    Think of it like applying a linear transformation to re-scale a raw
    score.
    """
    z = to_z(scores, pop_mean, pop_std)
    std_params = standard_parameters()
    return z_to_score(z, std_params["mean"], std_params["std"])


def standard_error_of_estimation(pop_std, pop_r):
    """This statistic is the standard deviation of true scores if the observed
    score is held constant. That is, what is the range true scores given
    the observed score. This depends on two things:
    """
    pop_std, pop_r = _validate_inputs(**locals())
    return pop_std * np.sqrt(pop_r * (1 - pop_r))


def confidence_intervals(scores, pop_mean, pop_std, pop_r, width=0.95):
    """Calculates confidence intervals for an obseved score feature.
    They should be symmetric around the adjusted score, but shifted around
    the observed score (towards the mean)
    """
    scores, pop_mean, pop_std, pop_r, width = _validate_inputs(**locals())
    adjusted_scores = to_adjusted(scores, pop_mean, pop_r)
    se_est = standard_error_of_estimation(pop_std, pop_r)
    alpha = 1 - (1 - width) / 2.0
    z = norm.ppf(alpha)
    return se_est * -z + adjusted_scores, se_est * z + adjusted_scores
