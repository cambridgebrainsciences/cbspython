import cbspython as cbs
import argparse
from os import path

_FEATS_TO_EXCLUDE = [
    'errors', 'attempted', 'better_than_chance', 'accuracy'
]


def main():

    parser = argparse.ArgumentParser(description='CBS Data Parser')

    parser.add_argument(
        "input_file",
        help="The source data file."
    )

    parser.add_argument(
        "output_name",
        help="The name of the output file(s)."
    )

    parser.add_argument(
        "-o", "--outputs", type=str, nargs='+',
        choices=['csv', 'pickle', 'report', 'stdout'], default=['csv'],
        help="A space separated list of files types to output."
    )

    parser.add_argument(
        '--destination', type=str, default=None,
        help="Path to save output files (default: location of the input file)"
    )

    parser.add_argument(
        '-f', '--keep-fields', type=str, nargs='*', default=[],
        help=(
            "List of other fields (space separated) to keep from the source "
            "raw data file. This basically keeps those columns in the "
            "intermediate 'report' data structure."
        )
    )

    parser.add_argument(
        '-i', '--include-columns', type=str, nargs='*', default=[],
        help="List of other columns (space separated) to include as features"
    )

    parser.add_argument(
        '-x', '--exclude-features', type=str, nargs='*', default=[],
        help="List of features (space separated) to drop"
    )

    parser.add_argument(
        '-n', "--num-jobs", type=int, choices=range(-1, 8+1), default=-1,
        help="How many cores to use for exracting features? (default: -1)"
    )

    parser.add_argument(
        '-u', '--user-column', type=str, default="User Email",
        help="The column for User identifier (default: 'Email Address')"
    )

    parser.add_argument(
        "-e", "--strip-emails", default=False,
        action=argparse.BooleanOptionalAction,
        help=(
            "If user IDs are stored as email addresses, setting this to True "
            "will strip the \"@domain.com\"  - leaving only the username."
        )
    )

    parser.add_argument(
        '-k', '--keep-users', nargs='*', type=str, default=None,
        help="A comma-separated list of user IDs to keep (drops all others)"
    )

    parser.add_argument(
        '-d', '--drop-users', nargs='*',  type=str, default=None,
        help="A comma-separated list of user IDs to drop (keeps all others)"
    )

    parser.add_argument(
        '-l', '--limit', type=int, default=None,
        help="Process only the first N scores"
    )

    parser.add_argument(
        '--drop-session-data', default=False,
        action=argparse.BooleanOptionalAction,
        help="Minimize memory usage by dropping the session data field?"
    )

    args = vars(parser.parse_args())

    def strip_email(e):
        return e[:e.index('@')] if '@' in e else e

    if args['strip_emails']:
        user_tfm = strip_email
    else:
        user_tfm = None

    report = cbs.Report.from_raw_data(
        args['input_file'],
        n_jobs=args['num_jobs'],
        extract_trials_data=False,
        drop_session_data=args['drop_session_data'],
        user_column=args['user_column'],
        keep_users=args['keep_users'],
        drop_users=args['drop_users'],
        limit=args['limit'],
        user_transfm=user_tfm,
        other_fields=args['keep_fields']
    )

    report.data = report.data.set_index(['batch_name'], append=True)
    df = report.to_wideform(
        include_common=True,
        retain_columns=args['include_columns'],
        exclude=_FEATS_TO_EXCLUDE + args['exclude_features']
    )

    outpath, _ = path.split(args['input_file'])
    if args['destination'] is not None:
        outpath = args['destination']

    if 'csv' in args['outputs']:
        df.to_csv(path.join(outpath, f"{args['output_name']}.csv.bz2"))

    if 'pickle' in args['outputs']:
        df.to_pickle(path.join(outpath, f"{args['output_name']}.pickle"))

    if 'report' in args['outputs']:
        report.to_pickle(
           path.join(outpath, f"{args['output_name']}.report.pickle")
        )

    if 'stdout' in args['outputs']:
        print(df)

    pass


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
