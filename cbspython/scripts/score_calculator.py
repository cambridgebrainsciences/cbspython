import cbspython as cbs
import pandas as pd
import numpy as np
import argparse
from pathlib import Path
from scipy import linalg


def main():
    parser = argparse.ArgumentParser(
       description='CBS Score Calculator', prog='score_calculator',
       formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "score_data_filename",
        help=(
            "The filename of the .csv that contains subjects\' "
            "CBS test scores. Must be a raw .csv file exported from CBS."
        )
    )

    parser.add_argument(
        "subject_info_filename",
        help=(
            "The filename of the .csv that contains subjects' demographic "
            "information. The file should contain one row per subject, with "
            "their CBS identifer, age, and gender in adjacent columns. Column "
            "headers should be 'User', 'Age', 'Gender'. The 'Gender' field "
            "should contain only 'Male' or 'Female'."
        )
    )

    parser.add_argument(
        '-n', '--name',
        help=(
            "You can provide an optional 'analysis name' or identifier for "
            "the set of files created by the score calculator. By default, "
            "will use the the score_data_filename."
        )
    )

    parser.add_argument(
        '-i', '--input-directory',
        help=(
            "The path where your input data files (required arguments) are "
            "found. For best results, should be an absolute path from the "
            "root of the filesystem. A value of None indicates the current "
            "working directory."
        )
    )

    parser.add_argument(
        '-o', '--output-directory',
        help=(
            "The path where your output data files will be saved. For best "
            "results, should be an absolute path from the root of the "
            "filesystem. A value of None indicates that files will be saved "
            "in the same location as the input files."
        )
    )

    parser.add_argument(
        '-b', '--age-bin-width', type=int, required=False,
        choices=range(1, 100), metavar="[1-100]", default=3,
        help=(
            "The width of the age bins in years, centred around each "
            "subjects' age, for creating age-matched norms."
        )
    )

    parser.add_argument(
        '-m', '--min-bin-size', type=int, required=False,
        choices=range(1, 1000), metavar="[1-1000]", default=50,
        help=(
            "The minimum number of data points that must be within each age "
            "bin. If there are fewer than this number, then the bin width is "
            "increased by 1 year until the minimum number of data points has "
            " been reached."
        )
    )

    args = vars(parser.parse_args())
    idx = pd.IndexSlice

    if args['name'] is None:
        args['name'] = Path(args['score_data_filename']).stem

    if args['input_directory'] is None:
        args['input_directory'] = Path.cwd()
    else:
        args['input_directory'] = Path(args['input_directory'])

    if args['output_directory'] is None:
        args['output_directory'] = args['input_directory']
    else:
        args['output_directory'] = Path(args['output_directory'])

    data_file = str(Path.joinpath(
        args['input_directory'],
        args['score_data_filename']
    ))
    report = cbs.Report(data_file)
    print(
        f"\tFound {report.num_subjects} subjects, "
        f"and {report.data.shape[0]} scores."
    )
    all_scores = report.to_wideform()

    info_file = str(Path.joinpath(
        args['input_directory'],
        args['subject_info_filename']
    ))
    print(f"Loading {info_file}...")
    subj_data = pd.read_csv(info_file)

    for c in ['user', 'age', 'gender']:
        assert c in subj_data.columns

    subj_data = subj_data.set_index('user')

    print(f"\tFound info on {subj_data.shape[0]} subjects.")

    # Create cumstomized norms for every score (not user).
    # Will have the same # of rows as the wideform score dataframe.
    norms = cbs.Norms()
    pop_norms = norms.query()

    features_in_norms = pop_norms[0].xs('count', axis=1).columns
    all_scores = (
        all_scores
        .reindex(features_in_norms, axis=1)
        .loc[:, features_in_norms]
    )

    matched_norms = []
    matched_covar = {}
    drop_subjects = []

    print("Creating Matched Norms For Each Subject...")
    for r_index, _ in all_scores.iterrows():
        user_name = r_index[0]
        if subj_data.index.__contains__(user_name):
            subj_info = subj_data.loc[user_name, :]
            subj_norms = norms.query(
                age=subj_info['age'],
                gender=subj_info['gender'],
                bin_size=args['age_bin_width'],
                min_bin_size=args['min_bin_size']
            )
            matched_norms.append(subj_norms[0])
            matched_covar[r_index] = subj_norms[1]
        else:
            print(f"WARNING: subject info for {user_name} not found; dropping "
                  f"this subject.")
            drop_subjects.append(user_name)

    all_scores = all_scores.drop(index=drop_subjects)
    subj_idx = all_scores.index
    scores_idx = all_scores.columns

    matched_norms = (
        pd
        .concat(matched_norms, ignore_index=True)
        .set_index(subj_idx)
    )

    matched_means = matched_norms.xs('mean', level=0, axis=1)
    matched_stds = matched_norms.xs('std', level=0, axis=1)

    print("Calculating Matched Z-Scores...")
    matched_z = (all_scores.values - matched_means) / matched_stds
    matched_z = pd.DataFrame(
        data=matched_z, index=subj_idx, columns=scores_idx)

    print("Calculating Raw Domain Scores and Matched Norms...")
    # To calculate raw domain scores, we need to convert the test scores into
    # z-scores relative to the population (rather than matched z-scores).
    pop_means = pop_norms[0].xs('mean', level=0, axis=1).to_numpy()
    pop_stds = pop_norms[0].xs('std', level=0, axis=1).to_numpy()
    pop_stdi = linalg.inv(np.diag(np.squeeze(pop_stds)))  # inverse of stds

    pop_z = (all_scores.to_numpy() - pop_means) / pop_stds
    pop_z = pd.DataFrame(data=pop_z, index=subj_idx, columns=scores_idx)

    # Create a numpy row/column index so that we can effiently slice (square)
    # covariance matrices.
    domain_msk = np.array(
        [f in cbs.domain_feature_list() for f in features_in_norms]
    )
    domain_ix = np.ix_(domain_msk, domain_msk)

    # The population correlation matrix (standardized covariance matrix) is
    # used to re-weight the PCA loadings in the presence of missing tests.
    # Let's pre-compute it and pre-slice the correct features.
    pop_r = np.dot(pop_stdi, np.dot(pop_norms[1].values, pop_stdi))
    pop_r = pop_r[domain_ix]

    # Init the domain score dataframe, and matched norms
    d_scores = pd.DataFrame(
        index=subj_idx, columns=cbs.DOMAIN_NAMES+['recipe']
    )
    d_matched = pd.DataFrame(
        index=subj_idx,
        columns=pd.MultiIndex.from_product([['mean', 'std'], cbs.DOMAIN_NAMES])
    )

    # These are the score features that are used in domain score calculation.
    # There are 12 features (1 per test)
    d_fvals = pop_z.loc[:, cbs.domain_feature_list()]

    # Recipes indicate which tests are included in the domain score calculation
    # because it is possible to have missing test scores.
    d_scores.loc[:, 'recipe'] = cbs.domain_recipes_from_inputs(d_fvals)

    # Matched means need to be converted into z-scores for calculation of
    # domain score norms. Also, pre-slice only the features (columns) needed.
    matched_means = (matched_means - pop_means) / pop_stds
    matched_means = matched_means.loc[:, cbs.domain_feature_list()]

    # Similarly, we also need to standardize (normalize?) and pre-slice each
    # user's matched sample covariance matrix
    for user, covariance in matched_covar.items():
        std_covariance = np.dot(pop_stdi, np.dot(covariance, pop_stdi))
        matched_covar[user] = std_covariance[domain_ix]

    # Calculate domain scores for all users at once (by recipe type)
    # At the same time, lets calculated matched norms for each domain score
    # recipe, for each of the users.
    for recipe in d_scores['recipe'].unique():
        users = d_scores[d_scores.recipe == recipe].index.tolist()
        f_mask = cbs.domain_input_mask(recipe)
        weights = cbs.domain_weights(f_mask, pop_r)
        scores = np.dot(d_fvals.loc[users, f_mask], weights)
        d_scores.loc[users, cbs.DOMAIN_NAMES] = scores

        f_means = matched_means.loc[users, f_mask].to_numpy()
        f_index = np.ix_(f_mask, f_mask, np.repeat(True, len(users)))
        f_covs = np.dstack([matched_covar[u] for u in users])
        f_covs = f_covs[f_index]

        r_norms = cbs.linear_combination_of_RVs(f_means, f_covs, weights)
        r_stds = np.vstack([
            np.sqrt(np.diag(r_norms[1][:, :, u])) for u in range(len(users))
        ])
        d_matched.loc[users, idx['mean', :]] = r_norms[0]
        d_matched.loc[users, idx['std', :]] = r_stds

    print("Calculating Matched Domain Z-Scores...")
    d_matched_z = d_scores.copy()
    d_matched_means = d_matched.loc[:, idx['mean', :]].to_numpy()
    d_matched_stds = d_matched.loc[:, idx['std', :]].to_numpy()
    d_score_vals = d_scores[cbs.DOMAIN_NAMES].to_numpy()

    d_matched_z[cbs.DOMAIN_NAMES] = (
        (d_score_vals-d_matched_means) / d_matched_stds
    )

    all_scores.to_csv(str(
        Path.joinpath(
            args['output_directory'],
            f"{args['name']}_raw_scores.csv"
        )
    ))

    matched_norms.to_csv(str(
        Path.joinpath(
            args['output_directory'],
            f"{args['name']}_matched_norms.csv"
        )
    ))

    matched_z.to_csv(str(
        Path.joinpath(
            args['output_directory'],
            f"{args['name']}_matched_z-scores.csv"
        )
    ))

    d_scores.to_csv(str(
        Path.joinpath(
            args['output_directory'],
            f"{args['name']}_domain_scores.csv"
        )
    ))

    d_matched.to_csv(str(
        Path.joinpath(
            args['output_directory'],
            f"{args['name']}_matched_domain_score_norms.csv"
        )
    ))

    d_matched_z.to_csv(str(
        Path.joinpath(
            args['output_directory'],
            f"{args['name']}_matched_domain_z-scores.csv"
        )
    ))

    print("DONE")


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
