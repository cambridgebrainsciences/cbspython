# Calculates the MD5 hash of the source data file for MeaningfulChangeModel
# Useful for updating cbshealthcare_test:test_src_data_MD5

from cbspython import MeaningfulChangeModel as MCM
import hashlib

md5 = hashlib.md5()
with open(MCM.default_src_file(), "rb") as f:
    for byte_block in iter(lambda: f.read(4096), b""):
        md5.update(byte_block)

print(md5.hexdigest())
