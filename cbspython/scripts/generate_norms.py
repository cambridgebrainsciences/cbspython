""" This script is used to generate norms that are used throughout the CBS
    python package. It is not intended for general use; rather, it is included
    so that users can see how the norms are generated. Without the raw data
    bundled into the CBS package, this script will not run.
"""

import cbspython as cbs
import pandas as pd
import numpy as np
import argparse
import operator
from os import path
from scipy import linalg
from scipy.stats import zscore

idx = pd.IndexSlice

_DATA_FILE = 'twelve_pillars_all_super_clean.pickle.bz2'
_AGE_BIN_EDGES = np.arange(8, 99, 1)
_AGE_BIN_NAMES = _AGE_BIN_EDGES[1:]

def main():
    parser = argparse.ArgumentParser(
                description='CBS Norms Generator', prog='generate_norms')
    parser.add_argument('--dropna', dest='dropna', action='store_true')
    parser.add_argument('--no-dropna', dest='dropna', action='store_false')
    parser.add_argument('--outfile', default='summary_norms_1yr_bins.csv')
    parser.set_defaults(dropna=True)
    args = parser.parse_args()

    # Load the source data file that has individual subject data, in a 
    # clean form (already filtered, etc.)
    src_file = path.join(path.dirname(__file__), '..', 'data', _DATA_FILE)
    src_data = pd.read_pickle(src_file)
    src_data = src_data.rename(columns=cbs.rename_legacy_columns())

    # Create our age_bin categorical variable
    src_data['age_bin'] = pd.cut(src_data['age'], _AGE_BIN_EDGES, 
        labels=_AGE_BIN_NAMES)
    
    # If we are dropping nans, then that means the norms are based off only
    # subjects that completed (and have good data for) every test.
    if args.dropna:
        src_data = src_data.dropna()

    # Take the demographics data, reformat to new DF with proper indexing
    demos = src_data[['age_bin', 'gender']]
    demos = pd.concat([demos], keys=['demographics'], axis=1)

    # Take all scores from the legacy DF, and re-index the columns with 
    # updated CBS test and feature names (nested index).
    all_scores = src_data[cbs.all_legacy_score_names()]
    all_scores.columns = pd.MultiIndex.from_tuples(cbs.all_features())
    
    # Join the demographics and scores, by existing (subject) index
    src_data = pd.concat([demos, all_scores], axis=1)

    # Create groupings by age and gender so that we can summarize, for each:
    # test feature mean, count, std, and covariance matrix.
    by_age_and_gender = src_data.groupby([
        ('demographics', 'age_bin'), ('demographics', 'gender')])
    
    # The full row-wise index has levels for:
    #   1) age, 2) gender, 3) test, 4) feature
    all_idx_labels = [operator.add((age, gender), t_idx)
                      for age in _AGE_BIN_NAMES
                      for gender in src_data[('demographics', 'gender')].cat.categories
                      for t_idx in cbs.all_features()]
    full_index = pd.MultiIndex.from_tuples(all_idx_labels)

    test_summaries = by_age_and_gender.agg(['mean', 'count', 'std'])
    test_summaries = test_summaries.stack(level=[0, 1])
    test_summaries = pd.concat([test_summaries], keys=['stat'], axis=1)
    test_summaries = test_summaries.reindex(full_index, fill_value=np.nan)

    # Zero-mean the score columns before computing covariance.
    overall_mn = src_data.loc[:, idx[cbs.TESTS.keys(), :]].mean(axis=0)
    src_data.loc[:, idx[cbs.TESTS.keys(), :]] -= overall_mn
    covariance = by_age_and_gender.cov(min_periods=2)
    covariance = covariance.reindex(full_index, fill_value=0)
    final_form = pd.concat([test_summaries, covariance], axis=1)
    
    # Clean up the index, add a level for the data source
    final_form.index = final_form.index.set_names(
        ['age', 'gender', 'test', 'feature'])
    final_form = pd.concat([final_form], keys=['legacy'], names=['source'])
    final_form = final_form.reset_index()
    out_file   = path.join(path.dirname(__file__), '..', 'data', args.outfile)
    final_form.to_csv(out_file, index=False)

main()
