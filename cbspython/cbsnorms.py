import pandas as pd
import numpy as np
from os import path
from .cbspython import pooled_vars_and_means, default_data_directory
from .cbstests import TESTS, test_names

idx = pd.IndexSlice


class Norms:
    """A class for performing dynamic queries of normative data"""

    _POOL_IDX = ['source', 'age', 'gender']
    _SCORE_IDX = ['test', 'feature']
    _IDX_LEVELS = _POOL_IDX + _SCORE_IDX
    _COL_LEVELS = _SCORE_IDX
    _SLICE_OPTS = ['bin_size', 'min_bin_size']

    @property
    def data_directory(self):
        return default_data_directory()

    def __init__(self, src_file='summary_norms_1yr_bins.csv'):
        file_name = path.join(self.data_directory, src_file)
        self.data = pd.read_csv(file_name, header=[0, 1],
                                index_col=list(range(len(self._IDX_LEVELS))))
        self.data.index = self.data.index.set_names(self._IDX_LEVELS)
        self.data = self.data.sort_index(
            level=self._IDX_LEVELS[0:2],
            sort_remaining=False
        )
        self.data = self.data.fillna(value=0)

    def data_slice(self, **kwargs):
        """Returns a slice of the data frame, give some index levels
        Args:
            args: a dict containing index labels (keys) and levels (values)
        Returns:
            A slice of the normative data frame that contains only the data
            cells of interest.
        Examples:
            >>> x.data_slice(test='digit_span', feature='avg_score', age=20})
        """

        # Check that passed keyword arguments are valid
        for arg in kwargs.keys():
            if arg not in (self._IDX_LEVELS+self._COL_LEVELS+self._SLICE_OPTS):
                raise ValueError(f"Invalid query argument: {arg}")

        # Remove KW arguments that have None as the value
        kwargs = {k: v for k, v in kwargs.items() if v is not None}

        # If an age bin size is specified, and age is provided, and it's
        # numeric, then convert the age into an age range (slice object).
        if ('bin_size' in kwargs
                and 'age' in kwargs
                and isinstance(kwargs['age'], (int, float, np.int64))):
            bin_lo = kwargs['age'] - kwargs['bin_size']/2
            bin_hi = kwargs['age'] + kwargs['bin_size']/2
            kwargs['age'] = slice(bin_lo, bin_hi, 1)

        # Age works better as a slice object, instead of a single int for now.
        if ('age' in kwargs and not isinstance(kwargs['age'], (slice))):
            kwargs['age'] = slice(kwargs['age'], kwargs['age'], 1)

        r_index = [
            kwargs.get(label, slice(None)) for label in self._IDX_LEVELS
        ]

        c_index = [
            kwargs.get(label, slice(None)) for label in self._COL_LEVELS
        ]

        data_slice = self.data.loc[tuple(r_index), tuple(c_index)]

        # Check that there is enough data for each included score feature
        if 'min_bin_size' in kwargs:
            data_per_score = self.num_data_points(data_slice)
            while (data_per_score < kwargs['min_bin_size']).any():
                age = r_index[1]
                r_index[1] = slice(age.start-1, age.stop+1, 1)
                data_slice = self.data.loc[tuple(r_index), :]
                data_per_score = self.num_data_points(data_slice)

        return data_slice

    def num_data_points(self, data_frame):
        """ Calculate the number of data points per score feature in the
            provided data frame.

        Arguments:
            data_frame {Pandas DataFrame} -- should be a dataframe that
            conforms to the shape of a Norms() data frame. E.g., one returned
            by norms.data_slice().
        """
        counts = data_frame.xs('count', level=1, axis=1)
        counts = counts.unstack(self._SCORE_IDX)
        return counts.sum(axis=0)

    def query(self, **kwargs):
        """ Performs a query of the data, and pools summary statistics (mean,
            std, covariance, etc.) from multiple selected subgroups.

        Arguments:
            kwargs: a dict containing index labels (keys) and levels (values)

        Returns:
            A dict that contains the new (weighted) mean, standard deviation,
            and count. TODO: UPDATE ALL THIS
        Examples:
            >>>
        """
        data = self.data_slice(**kwargs).dropna()
        stats = data.xs('stat', level=0, axis=1).unstack(self._SCORE_IDX)
        n_grps = stats.shape[0]
        n_meas = int(stats.shape[1] / 3)

        # Rather doing lots of dataframe slicing, we are going to manipulate
        # the data as a matrix - much faster. Reshape so X is a matrix like
        # this: 3 (stat) X M (# of measures) X G (# of groups).
        x = np.reshape(stats.to_numpy(), [n_grps, 3, n_meas])
        x = np.transpose(x, [1, 2, 0])

        # The first dimension is in [] to present the 3D shape of these data,
        # because the 3rd dimension indicates the group.
        g_counts = x[0, :, :]
        g_means = x[1, :, :]
        g_covar_ = np.reshape(
            data.iloc[:, 3:].values, [n_grps, n_meas, n_meas]
        )
        g_covar_ = np.transpose(g_covar_, [1, 2, 0])
        p_stats = pooled_vars_and_means(g_covar_, g_counts, means=g_means)
        p_counts = p_stats[0]
        p_means = p_stats[1]
        p_covar = p_stats[2]
        p_stds = np.sqrt(np.diag(p_covar))

        df_data = np.atleast_2d(np.concatenate([p_counts, p_means, p_stds]))
        df_final = pd.DataFrame(data=df_data, columns=stats.columns)
        for ind in self._POOL_IDX:
            df_final[ind] = ','.join(str(x) for x in stats.index.unique(ind))

        features = (
            data.index
            .droplevel(['source', 'age', 'gender'])
            .unique().to_list()
        )
        df_covar = pd.DataFrame(data=p_covar, index=features, columns=features)

        return (df_final, df_covar)


class ApplicationNorms(Norms):
    """ ApplicationNorms are a specialized version of norms used to generate
        and replicate the Norms used in the CBS web platform. Primary
        difference is that we don't worry about covariance. """

    _POOL_IDX = ['source', 'age', 'gender']
    _SCORE_IDX = ['test', 'feature']
    _IDX_LEVELS = _POOL_IDX
    _COL_LEVELS = _SCORE_IDX
    _SLICE_OPTS = ['bin_size', 'min_bin_size']

    def __init__(self, src_file='norms_master.pickle'):
        """ TO DO: ADD INTEGRITY CHECKS FOR DATA FILE """
        file_name = path.join(self.data_directory, src_file)
        all_norms = pd.read_pickle(file_name)
        self.data = pd.concat(all_norms.values(),
                              keys=all_norms.keys(), axis=1)

        if self.data.index.names[1] == 'age_at_test':
            self.data.index = self.data.index.rename('age', level=1)

        self.data.columns = self.data.columns.set_names(
            self._SCORE_IDX+['stat'])

        self.data = self.data.sort_index(level=self._IDX_LEVELS[0:2],
                                         sort_remaining=False)
        self.data = self.data.fillna(value=0)

    @property
    def age_bin_edges(self):
        """ NOT right edge inclusive
        """
        return np.array(
            [-np.Inf, 4, 6, 8, 10, 13, 16,
             18, 25, 35, 45, 55, 65, 75, 98, np.Inf]
        )

    def hc_age_bins(self, max_age=99):
        """Returns the age bins used by the CBS HC report.

        Args:
            max_age (int, optional): [description]. Defaults to 99.

        Returns:
            [type]: [description]
        """
        return [edge for edge in self.age_bin_edges[1:] if edge < max_age]

    def age_bins_for(self, ages):
        """ Given an age (or ages), returns the age bin for each.
        """
        ages = np.atleast_2d(ages)
        if ages.shape[0] == 1:
            ages = ages.T

        ages[np.isnan(ages)] = -np.Inf  # Replace missing ages with -Inf
        n_ages = ages.shape[0]
        lo_edg = np.tile(self.age_bin_edges[:-1], [n_ages, 1])
        hi_edg = np.tile(self.age_bin_edges[1:], [n_ages, 1])

        bin_id = (ages >= lo_edg) & (ages < hi_edg)
        lo_edg = lo_edg[bin_id]
        hi_edg = hi_edg[bin_id]
        return [slice(lo_edg[i], hi_edg[i]-1) for i in range(n_ages)]

    def norms_for(self, df):
        assert 'sex' in df.columns
        assert 'age' in df.columns

        df = df.copy()
        df['age_bin'] = self.age_bins_for(df['age'])
        norms = {
            i: self.query(age=r['age_bin'], gender=r['sex']) for
            i, r in df.iterrows()
        }
        norms = pd.concat(norms, names=df.index.names)
        norms.index = norms.index.droplevel(-1)
        return norms

    def query(self, **kwargs):
        """ Performs a query of the data, and pools summary statistics (mean,
            std, covariance, etc.) from multiple selected subgroups.

        Arguments:
            kwargs: a dict containing index labels (keys) and levels (values)

        Returns:
            A dict that contains the new (weighted) mean, standard deviation,
            and count. TODO: UPDATE ALL THIS
        Examples:
            >>>
        """
        stats = self.data_slice(**kwargs).dropna()
        n_grps = stats.shape[0]
        n_meas = int(stats.shape[1] / 3)

        # Rather doing lots of dataframe slicing, we are going to manipulate
        # the data as a matrix - much faster. Reshape so X is a matrix like
        # this: 3 (stat) X M (# of measures) X G (# of groups).
        x = np.reshape(stats.to_numpy(), [n_grps, n_meas, 3])
        x = np.transpose(x, [2, 1, 0])

        # The first dimension is in [] to present the 3D shape of these data,
        # because the 3rd dimension indicates the group.
        g_counts = x[0, :, :]
        g_means = x[1, :, :]
        g_vars_ = x[2, :, :] ** 2

        p_stats = pooled_vars_and_means(g_vars_, g_counts, means=g_means)
        df_data = np.array([p_stats[0], p_stats[1],  np.sqrt(p_stats[2])])
        df_data = np.atleast_2d(np.ravel(df_data.T))

        df_final = pd.DataFrame(data=df_data, columns=stats.columns)
        for ind in self._POOL_IDX:
            df_final[ind] = ','.join(str(x) for x in stats.index.unique(ind))

        return df_final


class ApplicationNorms2(ApplicationNorms):
    """ ApplicationNorms are a specialized version of norms used to generate
        and replicate the Norms used in the CBS web platform. This differes
        from the previous one because we load the data from the CBS src code
        directory. """

    _POOL_IDX = ['source', 'age', 'gender']
    _SCORE_IDX = ['test', 'feature']
    _IDX_LEVELS = _POOL_IDX
    _COL_LEVELS = _SCORE_IDX
    _SLICE_OPTS = ['bin_size', 'min_bin_size']

    def __init__(self, src_dir):
        """ TO DO: ADD INTEGRITY CHECKS FOR DATA FILE """
        csvs = {}
        for t, _ in TESTS.items():
            fn = path.join(src_dir, f"{t}_norms.csv")
            df = (
                pd.read_csv(fn)
                .rename(columns={'age_at_test': 'age'})
                .set_index(['source', 'age', 'gender', 'feature'])
            )
            csvs[t] = df

        def set_col_names(df, names):
            df.columns = df.columns.set_names(names)
            return df

        self.data = (
            pd.concat(csvs, names=['test'])
            .pipe(set_col_names, ['stat'])
            .unstack(['test', 'feature'])
        )

        self.data.columns = (
            self.data.columns
            .reorder_levels(['test', 'feature', 'stat'])
        )

        self.data = (
            self.data
            .sort_index(level=self._IDX_LEVELS[0:2], sort_remaining=False)
            .sort_index(axis=1, level=['feature', 'stat'])
            .loc[:, idx[test_names(), :, :]]
            .fillna(value=0)
        )
