from typing import Literal

ValidTaskNames = Literal[
    "spatial_span",
    "double_trouble",
    "grammatical_reasoning",
    "feature_match",
    "rotations",
    "paired_associates",
    "monkey_ladder",
    "token_search",
    "polygons",
    "spatial_planning",
    "sar",
    "digit_span",
    "odd_one_out",
]

ValidTaskAbbrevs = Literal[
    "SS", "DT", "GR", "FM", "RO", "PA", "ML", "TS", "PO", "SP", "SAR", "DS", "OOO"
]
