# This file stores all functions and classes requied to replicate the CBS
# "Healthcare Report" results. In general, these should not be used for
# research purposes.
import pandas as pd
import numpy as np
from .cbspython import default_data_directory, check_arg_value, iterable
from .cbstests import (
    hc_feature_list, abbrev_columns, HC_TESTS,
)

from os import path

idx = pd.IndexSlice


class MeaningfulChangeModel:

    _DEFAULT_SRC_FILENAME = 'CBS_HC_thresholds.csv.bz2'
    _BASELINE_TYPES = ['1st', 'n-1']
    _BOUNDS = ['low', 'high']
    _DIRECTIONS = ['decrease', 'increase', 'both']
    _VALID_TESTS = ['']

    @classmethod
    def default_src_file(cls):
        return path.join(default_data_directory(), cls._DEFAULT_SRC_FILENAME)

    def __init__(self, src_file=None, user_column='user',
                 dt_column='time_point'):
        """ cbspython MeaningfulChangeModel class constructor.

        Args:
            src_file (str, optional): source file (.csv) that contains
                thresholds for meaningful change, as exported from CBS Trials
                application. By default, use the provided data file contained
                in this package.
                Default: 'hc_mc_thresholds.csv.bz2'

            user_column (str, optional): the column of an input dataframe's
                index that will contain user identifiers.
                Default: 'user'

            dt_column (str, optional): the column of an input dataframe's index
                that will contain the time point identifiers.
                Defaults: 'time_point'
        """
        self._user = user_column
        self._dt = dt_column

        if src_file is None:
            src_file = self.default_src_file()

        # Read and process the exported thresholds data from CBStrials
        self._src_file = src_file
        self._data = (
            pd.read_csv(src_file)
            .rename(columns={'t1': self._dt})
            .set_index(['test', 't0', self._dt])
            .rename_axis('bound', axis=1)
            .unstack('test')
            .swaplevel(axis=1)
            .loc[:, idx[list(HC_TESTS.keys()), :]]
            .rename(
                columns={
                    n: f"{t.abbrev}_{t.hc_feature}" for
                    n, t in HC_TESTS.items()
                }, level='test'
            )
            .sort_index(axis=0)
            .sort_index(axis=1)
        )

        # Maximum dt (time point) value.
        self._dt_max = self._data.index.get_level_values(self._dt).max()+1

        # Split and apply multiindex to separate '1st' vs 'n-1' thresholds
        index_prev = [(i-1, i) for i in range(1+1, self._dt_max)]
        self._data = pd.concat([
            self._data.xs(1, level='t0'),
            self._data.loc[index_prev, :].droplevel('t0')
        ], keys=self._BASELINE_TYPES, names=['baseline'])

        # Make sure all time points are in the data frame (incuding t=1)
        self._data = self._data.reindex(
            ([(b, i) for b in ['1st', 'n-1'] for i in range(1, self._dt_max)])
        )

        # Number of dt points (time points) and tests
        self._num_dt = len(self._data.index.unique(self._dt))
        self._num_tests = len(self._data.columns.unique('test'))

        # Dictionary to map index labels on to rows or column names
        self._axis_map = {
            name: i for i, axis in enumerate(self._data.axes) for
            name in axis.names
        }

    def _groupers(self, df):
        """ Returns a list of index level names used for grouping.

        Args:
            df (Pandas DataFrame): The input dataframe.

        Returns:
            list: A list of names that can be used for doing groupby operations
        """
        return [col for col in df.index.names if col != self._dt]

    def _validate_input_dataframe(self, df):
        """ Validates an input dataframe, and massages it if required. Checks
            shape, names etc. If the column index is two levels (i.e., a
            MultiIndex of (test_name, feature) format) then it will be
            converted to "abbrev_feature" format. Only columns that are valid
            CBS HC features (e.g., "DS_avg_score") will be retained.

        Args:
            df (Pandas DataFrame): The input dataframe.

        Raises:
            Exception: if the df column index has more than two levels.

            Exception: if the required user and dt columns are not in the
                dataframe index.

            Exception: if one or more users have fewer than 2 scores.

            Exception: if none of the columns are valid CBS HC features.

        Returns:
            Pandas DataFrame: The input dataframe, massaged if required.
        """
        if len(df.columns.names) == 2:
            df.columns = abbrev_columns(df)
        elif len(df.columns.names) > 2:
            raise Exception("Too many levels in column MultiIndex.")

        if self._user not in df.index.names:
            raise Exception(f"Index requires a '{self._user}' level.")
        elif self._dt not in df.index.names:
            raise Exception(f"Index requires a '{self._dt}' level.")

        user_counts = df.groupby(self._groupers(df)).size()
        if (user_counts < 2).any():
            users = user_counts.index[user_counts < 2]
            raise Exception(
                f"Users [{', '.join([str(u) for u in users])}] have fewer than"
                f" 2 data points in {self._dt}."
            )

        all_hc_features = hc_feature_list(abbreviated=True)
        feats = [f for f in all_hc_features if f in df.columns]
        if len(feats) == 0:
            raise Exception(
                f"No columns in dataframe have HC feature names. "
                f"Must contain at least one of: {', '.join(all_hc_features)}"
            )

        return df[feats]

    def _baseline_df(self, df, baseline):
        """ From a given dataframe of CBS test scores, generates the "baseline"
            dataframe of scores that are used to test for meaningful change.
            The "baseline" is simply the first score for every user (if
            baseline is '1st'), or the previous test score (if baseline is
            'n-1'). This is primarily a utility function used in the
            calculation of meaningful change flags, and likely doesn't have
            much utility outside of that purpose.

            The resulting baseline dataframe will be of the same dimensions as
            the input DF and with the same indices and column.

        Args:
            df (Pandas Dataframe): The input dataframe of CBS scores.

            baseline (str, optional): an option to specify the kind of
                baseline scores to use. Valid Values: ['1st', 'n-1']

        Returns:
            Pandas Dataframe: The baseline score dataframe.
        """
        check_arg_value('baseline', baseline, self._BASELINE_TYPES)

        t0 = []
        for _, user_data in df.groupby(self._groupers(df)):
            if baseline == '1st':
                user_data = (
                    user_data.iloc[0:2, :]
                    .shift()
                    .reindex_like(user_data, method='ffill')
                )
            elif baseline == 'n-1':
                user_data = user_data.shift()
            t0.append(user_data)
        t0 = pd.concat(t0)

        return t0

    def _delta_df(self, df, baseline):
        """ Calculates the set of "delta" scores. That is, the difference
            between users' test scores and their baseline score.

        Args:
            df (Pandas Dataframe): The input dataframe of CBS scores.

            baseline (str, optional): an option to specify the kind of
                baseline scores to use. Valid Values: ['1st', 'n-1']

        Returns:
            Pandas Dataframe: The delta scores as a dataframe. It will have the
                same dimensions, index, and columns as the input data.
        """
        return df - self._baseline_df(df, baseline)

    def _thresholds_df(self, df, baseline):
        """ Restructures the internal dataframe containing thresholds into the
            same format / structure as the input data. For example, if the
            baseline is '1st', then we repeate the N vs 1 thresholds for every
            user and time point.

        Args:
            df (Pandas DataFrame): The input data frame.

            baseline (str): The type of baseline. Valid Values: ['1st', 'n-1']

        Returns:
            Pandas DataFrame: A DF containing all the pre-calculated thresholds
                for meaningful change. The DF will have the same index as the
                input DF, but there will be twice as many columns and a new
                level on the column index - 'bound' (upper/lower).
        """
        df = self._validate_input_dataframe(df)
        thresholds = {}
        for user, user_data in df.groupby(self._groupers(df)):
            thresholds[user] = (
                self._data
                .xs(baseline)
                .loc[:, idx[df.columns, :]]
                .reindex(user_data.reset_index()[self._dt], method='ffill')
            )
        return pd.concat(thresholds, names=self._groupers(df))

    def analyze(self, df, baseline, direction='both'):
        """ Performs an analysis of scores to determine if users' CBS scores
            exhibit signicicant change either, A) from 1st to Nth play (e.g.,
            from 1st to 5th test play), or from B) from N-1 to Nth (e.g.,
            4th to 5th) test play.

            All scores in the provided dataframe are analyzed at the same time.

            Thresholds for meaningful change come directly from an export of
            these values from the CBS trials rails application (see SCI-28).

            The actual values themselves are derived from a dataset and
            analysis detailed in <update this here>.

        Args:

            df (Pandas Dataframe): The input dataframe of users' scores.
                Different scores are provided as separate columns (e.g.,
                ["DS_avg_score", "FM_final_score", etc]) such that each row of
                the dataframe belongs to a single user and time point. Each
                row must be indexed by 'user', and 'time_point'.

            baseline (str): Indicates how the baseline comparison is performed.
                That is, relative to each users' first scores ('1st') or to
                each users' previous scores ('n-1').
                Default: '1st'.

            direction (str): Direction for significant change?
                Valid Values: ['increase', 'decrease', 'both'].
                Default: 'both'
        """
        check_arg_value(
            'direction', direction, ['increase', 'decrease', 'both']
        )
        scores = self._validate_input_dataframe(df)
        deltas = self._delta_df(scores, baseline)
        thresholds = self._thresholds_df(scores, baseline)

        mc_flags = pd.concat([
            deltas > thresholds.xs('high', level='bound', axis=1),
            deltas < thresholds.xs('low', level='bound', axis=1)
        ], axis=1, keys=['increase', 'decrease'], names=[f"MC_{baseline}"])

        if direction != 'both':
            mc_flags = mc_flags.xs(direction, level=f"MC_{baseline}", axis=1)

        return mc_flags

    def get_thresholds(self, **opts):
        """ Returns the internal threshold data, or a subset specified by some
            set of variable input arguments.

        Args (Optional):
            Names and values of index / columns levels to slice from the
            internal data frame.

        Returns:
            Pandas DataFrame: The threshold values.

        Examples:
            >>> mcm.get_thresholds(baseline='1st', test=['DS_avg_score', 'FM_final_score'])  # noqa: E501

        """

        # Use boolean indexers on row and column indices to slice our _data
        # We will mask the indices based on the passed arguments.
        axis_ind = [
            np.full(len(self._data.index), True),
            np.full(len(self._data.columns), True)
        ]

        thresholds = self._data
        for opt_name, opt_vals in opts.items():
            check_arg_value('index_name', opt_name, self._axis_map.keys())
            iax = self._axis_map[opt_name]
            axis = self._data.axes[self._axis_map[opt_name]]
            check_arg_value(opt_name, opt_vals, axis.unique(opt_name))

            if not iterable(opt_vals):
                opt_vals = [opt_vals]

            axis_ind[iax] &= axis.get_level_values(opt_name).isin(opt_vals)

        return thresholds.loc[axis_ind[0], axis_ind[1]]
