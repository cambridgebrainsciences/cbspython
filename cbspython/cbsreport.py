import pandas as pd
import numpy as np
import datetime
from os import path
from .cbspython import to_camelcase
from .cbstests import TESTS, Test, all_features, all_feature_fields
from multiprocessing import get_context
from functools import partial
from enum import Enum, unique


idx = pd.IndexSlice


# Print iterations progress
# https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
def printProgressBar(
    iteration,
    total,
    prefix="",
    suffix="",
    decimals=1,
    length=100,
    fill="█",
    printEnd="\r",
):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + "-" * (length - filledLength)
    print("\r%s |%s| %s%% %s" % (prefix, bar, percent, suffix), end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


def parse_score_features(df, do_print=True):
    for i, (index, row) in enumerate(df.iterrows()):
        if do_print:
            printProgressBar(
                i + 1,
                df.shape[0],
                prefix="Parsing Score Features:",
                suffix="Complete",
                length=50,
            )
        test = TESTS[index[1]]
        feature_data = {}
        features_extracted = True
        try:
            feature_data.update(test.parse_raw(row["raw_score"]))
        except Exception:
            features_extracted = False
            if do_print:
                print(
                    f"WARNING: Invalid raw score format for row {row['id']}"
                    f" {index} with data ({row['raw_score']})"
                )

        if "session_data" in df.columns:
            try:
                feature_data.update(test.parse_session(row["session_data"]))
            except Exception:
                features_extracted = False
                if do_print:
                    print(
                        f"WARNING: Invalid session_data for row {row['id']}"
                        f" {index} with data ({row['session_data']})"
                    )

        # try:
        if features_extracted:
            try:
                feature_data.update(test.calculate_features(feature_data))
            except Exception as e:
                if do_print:
                    print(e)

        for feature_name, feature_value in feature_data.items():
            if feature_name in test.features(include_common=True):
                field = test.field_for_feature(feature_name)
                df.loc[index, field] = feature_value
    return df


def parse_trial_data(df, do_print):
    """For now, assumes which vars is 'timing' and 'difficulty'"""
    trial_data = pd.DataFrame()
    for i, (index, row) in enumerate(df.iterrows()):
        if do_print:
            printProgressBar(
                i + 1,
                df.shape[0],
                prefix="Parsing Trial Data:",
                suffix="Complete",
                length=50,
            )
        test = TESTS[index[1]]

        # Need to deal with non-saved session data; refetch and parse.
        # For now, assuming the session data is saved in the report.
        if "session_data" in df.columns:
            session_data = test.parse_session(
                row["session_data"], extract_features=False
            )

            if (len(session_data) > 0) and ("questions" in session_data):
                try:
                    x = test.extract_trial_data(session_data)
                    session_data = pd.concat({index: x}, names=df.index.names)

                    trial_data = pd.concat([trial_data, session_data], axis=0)

                except Exception:
                    if do_print:
                        print("\nINVALID TRIAL DATA")
                        print(index)
                        print(session_data)
                        print("\n")

    return trial_data


class Enum(Enum):

    @classmethod
    def codes(cls):
        return [e.value for e in cls]

    @classmethod
    def labels(cls):
        return [e.name for e in cls]


@unique
class DeviceType(Enum):
    BOT = 0
    TV = 1
    CONSOLE = 2
    TABLET = 3
    MOBILE = 4
    DESKTOP = 5


@unique
class Locale(Enum):
    EN = 0
    FR = 1
    NL = 2
    DE = 3
    ES = 4


class ReportParser:
    """A class for parsing CBS score CSVs (provided by Admin Interface)"""

    @property
    def og_columns_to_keep(self):
        """Columns in the original report spreadsheet that we will keep."""
        og_cols = [
            "Id",
            "User",
            "User Email",
            "Test",
            "Test Name",
            "Trial Name",
            "Batch",
            "Batch Name",
            "Time Point",
            "Screen Width",
            "Created At",
            "Score",
            "Valid",
            "Session ID",
            "Locale",
            "Session Data",
            "Device Type",
            self._raw_score_col,
            "Report",
        ]

        return [x for x in og_cols if x in self.data.columns]

    @property
    def og_column_rename_map(self):
        """A dict map to rename columns from their original, to new names."""
        og_map = {
            "Test": "test_id",
            "Score": "single_score",
            "User": "user_id",
            "Locale": "locale",
            self._raw_score_col: "raw_score",
        }

        return {k: og_map[k] for k in og_map.keys() if k in self.data.columns}

    @property
    def multi_index_columns(self):
        """Columns used to construct the MultiIndex"""
        return ["user", "test_name", self.session_link_id, "created_at"]

    @property
    def datetime_columns(self):
        """What columns in the report are Date/Times?"""
        return ["created_at"]

    @property
    def cluster_threshold(self):
        return self._cluster_threshold

    @cluster_threshold.setter
    def cluster_threshold(self, num_days):
        if num_days < 0:
            self._cluster_threshold = 0
        else:
            self._cluster_threshold = num_days
        self.cluster_scores()

    @property
    def source_file(self):
        return self._source_file

    @property
    def datestamp(self):
        return self._datestamp.strftime("%Y-%m-%d %H:%M:%S")

    def rprint(self, text):
        if self._verbose:
            print(text)

    def __init__(
        self,
        src,
        keep_repeats="first",
        delimiter=",",
        raw_column="Legacy Raw Score",
        user_column="User Email",
        keep_users=None,
        drop_users=None,
        user_transfm=None,
        cluster_threshold=2,
        extract_trials_data=False,
        drop_session_data=False,
        first_date=None,
        n_jobs=1,
        verbose=True,
        limit=None,
        do_cluster=True,
        other_fields=[],
    ):
        """

        Arguments:
            src_file {[type]} -- [description]

        Keyword Arguments:
            keep_repeats {str} -- [description] (default: {'first'})
            delimiter {str} -- [description] (default: {','})
            raw_column {str} -- [description] (default: {'Legacy Raw Score'})
            keep_users {[type]} -- [description] (default: {None})
            drop_users {[type]} -- [description] (default: {None})
            user_transfm {[type]} -- e.g, lambda x: x[0:x.index('@')]
                                        (default: {None})

        Raises:
            ValueError: [description]
        """
        if src.__class__ is str:
            _, file_ = path.split(src)
            if (file_[-4:] == ".csv") or (file_[-8:] == ".csv.bz2"):
                self.data = pd.read_csv(src, delimiter=delimiter)
            elif (file_[-7:] == ".pickle") or (file_[-11:] == ".pickle.bz2"):
                self.data = pd.read_pickle(src)
            else:
                raise TypeError(f"Not sure how to handle {src}")

            self._source_file = src
            self._datestamp = datetime.datetime.now()

        elif src.__class__ is pd.core.frame.DataFrame:
            self.data = src.copy()
            self._includes_session_data = "session_data" in self.data.columns
            self._has_link_id = "session_id" in self.data.index.names
            src = "DataFrame"

        else:
            raise TypeError(f"Invalid src type {src.__class__}")

        self._verbose = verbose

        self.rprint(f"{src} initially has {self.data.shape[0]} rows.")
        self.rprint("Restructing data frame ...")

        self._raw_score_col = raw_column
        self._includes_session_data = "Session Data" in self.data.columns
        self._has_link_id = ("Session ID" in self.data.columns) | (
            "Report" in self.data.columns
        )

        self.data = self.data[self.og_columns_to_keep + other_fields]
        self.data["user"] = self.data[user_column]
        self.data = self.data.rename(columns=self.og_column_rename_map)
        self.data = self.data.rename(
            columns={orig: to_camelcase(orig) for orig in self.data.columns}
        )
        self.data = self.data.dropna(subset=self.multi_index_columns + ["test_name"])

        # if limit is not None:
        #     self.data = self.data.iloc[0:limit, :]

        # Converts all test names to camel case. Pretty obvious I guess.
        self.data["test_name"] = self.data["test_name"].apply(to_camelcase)

        # Apply a function to transform user IDs, e.g., to strip "@cbs.com"
        if user_transfm:
            self.data["user"] = self.data["user"].apply(user_transfm)

        # Keeps only scores from users specified in keep_users
        if keep_users is not None:
            self.data = self.data[self.data["user"].isin(keep_users)]

        # Drops scores from all users specified in drop_users
        if drop_users is not None:
            self.data = self.data[~self.data["user"].isin(drop_users)]

        # Basic check that are data remaining after dropping users.
        if self.data.shape[0] == 0:
            raise Exception("Dataframe is empty.")

        # Convert datetime columns
        for column in self.datetime_columns:
            self.data[column] = pd.to_datetime(self.data[column])

        if (
            "device_type" in self.data.columns
            and self.data["device_type"].dtype == "str"
            and self.data["device_type"].str.isnumeric().all()
        ):
            self.data["device_type"] = pd.Categorical.from_codes(
                self.data["device_type"].astype("int32"), DeviceType.labels()
            )

        if (
            "locale" in self.data.columns
            and self.data["locale"].dtype == "str"
            and self.data["locale"].str.isnumeric().all()
        ):
            self.data["locale"] = pd.Categorical.from_codes(
                self.data["locale"].astype("int32"), Locale.labels()
            )

        # Applies a filter to remove scores before the first_date
        if first_date is not None:
            self.data = self.data[self.data["created_at"] >= first_date]

        # Setting the cluster threshold also sets the index!
        if do_cluster:
            self.rprint("Clustering scores ...")
            self.cluster_threshold = cluster_threshold
        else:
            self.data["test_date"] = np.nan
            self.set_index()
        # If there are repeated scores per time point, keep the first one.
        self.data = self.data[~self.data.index.duplicated(keep=keep_repeats)]

        # Initialize the raw fields
        self.data = pd.concat(
            [
                self.data,
                pd.DataFrame(
                    columns=[
                        f"raw_field_{i+1:02d}" for i in range(Test._NUM_RAW_FIELDS)
                    ],
                    index=self.data.index,
                ),
            ],
            axis=1,
        )

        # Initialize columns for common session-derived features
        self.data = pd.concat(
            [
                self.data,
                pd.DataFrame(
                    columns=Test.common_session_feature_calcs(), index=self.data.index
                ),
            ],
            axis=1,
        )

        # Batch by blocks, and parallelize
        df = self.data.iloc[0:limit, :]
        self.rprint(f"Parsing Score Features (n_cores={n_jobs})")
        if n_jobs > 0:
            dfs_ = np.array_split(df, n_jobs)
            pool = get_context("fork").Pool(n_jobs)
            df_chunks = pool.map(
                partial(parse_score_features, do_print=self._verbose), dfs_
            )
            self.data = pd.concat(df_chunks)
            pool.close()
            pool.join()
        else:
            self.data = parse_score_features(self.data, self._verbose)

        for col in Test._FEATURE_FIELD_NAMES + Test.common_session_features():
            self.data[col] = pd.to_numeric(self.data[col], errors="coerce")

        if extract_trials_data:
            self.trials_data = self.extract_trials_data(n_jobs=n_jobs)

        if self._includes_session_data and drop_session_data:
            self.data = self.data.drop(labels="session_data", axis=1)

    @property
    def session_link_id(self):
        """The name of the field that links scores together in a session.
        usually, this would be the session_id,  but if not available we
        estimate from the time_point id a testing dates.=
        """
        if "report" in self.data.columns:
            return "report"
        elif "session_id" in self.data.columns:
            return "session_id"
        else:
            return "time_point"

    def cluster_scores(self):
        """Assigns a cluster ID to every test score, to indicate which scores
        belong to the same session. Scores within some threshold of each
        other (e.g., 2 days) are considered part of the same cluster. After
        the threshold is exceeded, the cluster index is incrememented. The
        size of the cluster threshold is a property of the Report class,
        and can be set after the Report is generated.
        """
        self.reset_index()
        self.data["cluster"] = 0
        self.data = self.data.sort_values(["user", self.session_link_id, "created_at"])

        # Without a session_id field, we group scores based on time.
        if not self._has_link_id:
            time_threshold = pd.Timedelta(days=self._cluster_threshold)
            for _, subj_data in self.data.groupby(["user", "time_point"]):
                self.data.loc[subj_data.index, "cluster"] = (
                    subj_data["created_at"].diff() > time_threshold
                ).cumsum()

        self.data["test_date"] = pd.NaT
        for _, cluster_data in self.data.groupby(
            ["user", self.session_link_id, "cluster"]
        ):
            self.data.loc[cluster_data.index, "test_date"] = pd.to_datetime(
                (cluster_data["created_at"].iloc[0].date())
            )
        self.set_index()

    def extract_trials_data(self, n_jobs=8):
        df = self.data
        self.rprint(f"Extracting data from individual trials (n_cores={n_jobs})")
        if n_jobs > 0:
            dfs_ = np.array_split(df, n_jobs)
            pool = get_context("fork").Pool(n_jobs)
            df_chunks = pool.map(
                partial(parse_trial_data, do_print=self._verbose), dfs_
            )
            self.trial_data = pd.concat(df_chunks)
            pool.close()
            pool.join()
        else:
            self.trial_data = parse_trial_data(self.data)

        self.trial_data = self.trial_data.infer_objects()
        return self.trial_data

    def reset_index(self):
        self.data = self.data.reset_index()

    def set_index(self):
        self.data = self.data.set_index(self.multi_index_columns + ["test_date"])
        self.data = self.data.sort_index()
        self.data = self.data.reset_index(level="created_at")


class Report:

    def __init__(self, parsed_data):
        assert isinstance(parsed_data, pd.DataFrame)
        self.data = parsed_data

    @classmethod
    def from_pickle(cls, pickle_file):
        return cls(pd.read_pickle(pickle_file))

    @classmethod
    def from_raw_data(cls, src_file, **vargs):
        parser = ReportParser(src_file, **vargs)
        return cls(parser.data)

    @property
    def num_subjects(self):
        return len(self.data.index.unique(level="user"))

    @property
    def trial_names(self):
        """Returns an identifier (string) of trials included in the report."""
        trial_names = list(self.data["trial_name"].unique())
        return "_".join(trial_names).replace(" ", "_")

    def scores_for_user(self, user_id):
        return self.data.loc[idx[user_id, :, :, :], :]

    def to_wideform(
        self, include_common=False, exclude=[], retain_columns=[], add_to_index=[]
    ):
        """Converts the long form (one test score per row) data into a wide
            form data frame, where there is one row per subject timepoint, and
            all score features from all 12 tests are concatenated along the
            column axis. Keep only those columns that contain actual score
            data.

        Args:
            include_common (boolean): include all the common session-derived
                features for every test? e.g., num correct, num errors, etc.
                (default: False)
            exclude (list of strings): a list of features to exclude when
                pivoting the data frame. (default: [])
            retain_columns (list of strings): a list of other columns that
                will be retained in the pivoted dataframe. For example, if you
                want to keep the 'device_type' or 'locale' for each individual
                test score. (default: [])

        Returns:
            A pandas DataFrame, with one row per subject timepoint, and 12 x ?
            columns containing the score features. Score columns are renamed to
            use the feature name, rather than the feature field.
        """

        score_data = (
            self.data.set_index(add_to_index, append=True)
            .unstack("test_name")
            .swaplevel(axis=1)
        )

        full_map = list(
            zip(
                all_feature_fields(
                    include_common=include_common, inject=retain_columns
                ),
                all_features(include_common=include_common, inject=retain_columns),
            )
        )

        field_map = [f for f in full_map if f[0] in score_data.columns]

        selected_features = [f[1] for f in field_map if f[1][1] not in exclude]

        def set_column_names(df, names):
            df.columns = names
            return df

        score_data = (
            score_data.loc[:, [f[0] for f in field_map]]
            .pipe(
                set_column_names, pd.MultiIndex.from_tuples([f[1] for f in field_map])
            )
            .loc[:, selected_features]
        )

        return score_data

    def to_pickle(self, file):
        """Save to binary file"""
        self.data.to_pickle(file)
